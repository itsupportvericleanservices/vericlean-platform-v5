function updateClient(event, id) {
  event.preventDefault();

  var name = $("#name").val();
  var address = $("#address").val();
  var address2 = $("#address2").val();
  var state = $("#state").val();
  var city = $("#city").val();
  var zipcode = $("#zipcode").val();
  var phone = $("#phone").val();
  var email = $("#email").val();
  var contact_name = $("#contact_name").val();
  var option_active_site = $("#option_active_site").val(); // all or some


  var sites = []
  if(option_active_site == "some"){
    $('.check_modal_active_sites').each(function (i, site) {
      if ($(site).is(':checked')) {
        sites.push($(site).data('id'));
      }
    });
  }

  var active = 0
  if ($("#active").is(':checked')) {
    active = 1;
  }
  console.log(option_active_site)

  $.ajax({
    type: "POST",
    url: "../../../router/clients/update.php?id=" + id,
    data: JSON.stringify({
      name: name,
      address: address,
      address2: address2,
      state: state,
      city: city,
      zipcode: zipcode,
      phone: phone,
      email: email,
      contact_name: contact_name,
      active: active,
      active_sites: option_active_site,
      branches_id: sites
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Customer Updated",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../clients/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}


function active_sites(){
  var active = 0
  if ($("#active").is(':checked')) {
    active = 1;
  }

  //muestra las opciones all or some cuando queremos actualizar el customer
  if(active == 1){
    $("#row_active_sites").show();
  }else{
    $("#row_active_sites").hide();
  }
}

$(document).ready(function () {
  $("#message").hide()
});


function options_update_site(event, option){
  $("#option_active_site").val(option)
  if (option == "all") {
    $("#message").show()
  }
  //si solo algunos sites quiero habilitar, muestreme el modal de todos esos sites.
  if(option == "some"){
    $('#sitesModal').modal('show');
    $("#message").hide()
  }
}