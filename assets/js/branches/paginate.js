function paginate(event, action,page,filter) {
  if (event != null) {
    event.preventDefault();
  }
  
  var filtro = JSON.parse(window.localStorage.getItem('filter_by'))
  $(".loading").show()
  var searchstr = $(".searchstr").val();

  var dataView = parseInt($("#page").val())

  if (action == 'next') {
    var pageData = dataView + 1
    window.localStorage.setItem('page', JSON.stringify(pageData));
    $.ajax({
      type: "POST",
      url: "../../../router/branches/index.php",
      data: JSON.stringify({
        page: pageData ,
        searchstr: searchstr,
        filtro:filtro
      }),
      dataType: "html"
    }).done(function (data) {
      $(".loading").hide()
      $("#m-portlet__body").html(data);
    });
  }
  if (action == 'back') {
    if (dataView > 1) {
      var pageData = dataView - 1
      window.localStorage.setItem('page', JSON.stringify(pageData));
      $.ajax({
        type: "POST",
        url: "../../../router/branches/index.php",
        data: JSON.stringify({
          page: pageData,
          searchstr: searchstr,
          filtro:filtro
        }),
        dataType: "html"
      }).done(function (data) {
        $(".loading").hide()
        $("#m-portlet__body").html(data);
      });
    }
  }
  if (action == 'get') {
      $.ajax({
        type: "POST",
        url: "../../../router/branches/index.php",
        data: JSON.stringify({
          page:  page,
          searchstr: filter,
          filtro:filtro
        }),
        dataType: "html"
      }).done(function (data) {
        $(".loading").hide()
        $("#m-portlet__body").html(data);
      });
  }
}
