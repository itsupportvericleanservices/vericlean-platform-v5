
function reassigneModal (event, cleaner_id, branch_id, site_name){
  event.preventDefault();
  $('#branch_reassinged_name').val(site_name);
  $('#branch_reassinged_id').val(branch_id);
  $('#cleanersModal').modal('show');
}
function selectCleanerReassinged(event){
  event.preventDefault();
}
function getCleaners(event, id_table) {
  event.preventDefault();

  //enviar ajax para llenar el select
  $.ajax({
    type: "POST",
    url: "../../../router/cleaners/index.php",
    dataType: "json"
  }).done(function (data) {
    data.forEach(element => {
      $('.cleaner_id').append('<option value="' + element.id + '">' + element.first_name + '</option>');
    });
    $(".listCleanerReassingned" + id_table).css('display', 'block');
  });

}


function reassignedCleaners(event) {
  event.preventDefault();

  var cleaner_id = $("#selectCleanerReassinged").val();
  var branch_id = $("#branch_reassinged_id").val()
  console.log(branch_id)
  $.ajax({
    type: "POST",
    url: "../../../router/route_histories/reassigned.php",
    data: JSON.stringify({
      assigned_id: cleaner_id,
      user_id: cleaner_id,
      branch_id: branch_id,
      scheduled_date: $('#date_filter').val()
    }),
    dataType: "json"
  }).done(function (data) {
    // users/:id/token_notification

    fetch(`http://52.52.229.187/users/${cleaner_id}/token_notification`, {
              method: "GET",
              headers: {"Content-Type": "application/json"}
          })
          .then(res => res.json())
          .then(data => {
             console.log("object");   
            console.log(data.token)
            autoSendPushReassigned(data.token,"you have a reassignment")
        })    

    //$(".listCleanerReassingned" + id_table).css('display', 'none');
    swal({
      title: "Success!",
      text: "Cleaner Reassingned",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../routes_cleaners/index.php";
    });
  });

}

function autoSendPushReassigned(token, message){
  var key = 'AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q';
  var to = token;
  var notification = {
    'title': 'Vericlean',
    'body': message
  };

  fetch('https://fcm.googleapis.com/fcm/send', {
    'method': 'POST',
    'headers': {
      'Authorization': 'key=AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q',
      'Content-Type': 'application/json'
    },
    'body': JSON.stringify({
      'data': notification,
      'to': to
    })
  }) 
  .then(res => res.json())
  .then(data => {
                
    console.log(data)  
  })  
}



