
// $(document).ready(function(){
//   var page = 1
//   var searchstr = window.localStorage.getItem('filtro_sites');
//   console.log(searchstr);

//   // if (searchstr != null) {
//   //   console.log("object")
//     $.ajax({
//       type: "POST",
//       url: "../../../router/branches/index.php",
//       data: JSON.stringify({
//         page: page,
//         searchstr: encodeURI(searchstr)
//       }),
//       dataType: "html"
//     }).done(function (data) {
//       $("#m-portlet__body").html(data);
//       console.log(data)
//     });
//   // }
// })

function getBranches(page) {
  $(".loading").show()
  window.localStorage.removeItem('filtro_sites');
  window.localStorage.removeItem('filter_by');
  window.localStorage.removeItem('value');
  window.localStorage.removeItem('page');
  $.ajax({
    type: "POST",
    url: "../../../router/branches/index.php",
    data: JSON.stringify({
      page: page,
      filter_by: null,
      value:null
    }),
    dataType: "html"
  }).done(function (data) {
    $(".loading").hide()
    $("#m-portlet__body").html(data);
  });

}

function branches_filter_by(filtro,val) {
  var filter_by = $(".filter_by").val();

  if (filtro != null) {
    filter_by = filtro
  }

  if (filter_by == "address" || filter_by == "site_name" || filter_by == "site_code") {

    $(".filter_by_custom").show();
    $(".filter_by_client").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".clear").show();
    $(".filter").show();
  }
  if(filter_by == "client") {
    $(".option_remove").remove()

    $(".filter_by_client").show();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    $(".clear").show();
    $(".filter").hide();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json",
    }).done(function (data) {
      $('.client_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        var select= ''
        if (element.id == Number(val)) {
          select = 'selected'
        }
        $('.client_id').append('<option value="' + element.id + '"class="option_remove" '+ select +' >'+  element.name +'</option>');
      });
    });
  }
  if(filter_by == "customer") {
    $(".option_remove").remove()

    $(".filter_by_customer").show();
    $(".filter_by_client").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    $(".clear").show();
    $(".filter").hide();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/indirect_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      $('.customer_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        var select= ''
        if (element.id == Number(val)) {
          select = 'selected'
        }
        $('.customer_id').append('<option value="' + element.id + '" class="option_remove" '+ select +' >' + element.name + '</option>');
      });
    });
  }
  if(filter_by == "cleaner") {
    $(".filter_by_cleaner").show();
    $(".filter_by_customer").hide();
    $(".filter_by_client").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    $(".clear").show();
    $(".filter").hide();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/cleaners/index.php",
      dataType: "json"
    }).done(function (data) {
      $('.cleaner_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        var select= ''
        if (element.id == Number(val)) {
          select = 'selected'
        }
        
        $('.cleaner_id').append('<option value="'+element.id+'" '+ select +' >'+element.first_name+' '+element.last_name+'</option>');
      });
    });
  }
  if(filter_by == "supervisor") {
    $(".filter_by_supervisor").show();
    $(".filter_by_cleaner").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_client").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    $(".clear").show();
    $(".filter").hide();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/supervisors/index.php",
      dataType: "json"
    }).done(function (data) {
      $('.supervisor_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        var select= ''
        if (element.id == Number(val)) {
          select = 'selected'
        }
        $('.supervisor_id').append('<option value="'+element.id+'" '+ select +' >'+element.first_name+' '+element.last_name+'</option>');
      });
    });
  }
  if(filter_by == "state") {
    $(".filter_by_state").show();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_client").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    $(".clear").show();
    $(".filter").hide();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/branches/states.php",
      dataType: "json"
    }).done(function (data) {
      console.log(data)
      $('.state').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        var select= ''
        if (element.id == Number(val)) {
          select = 'selected'
        }
        if(element.state !== null){
          $('.state').append('<option value="'+element.state+'" '+ select +' >'+element.state+'</option>');
        }
      });
    });
  }
  if(filter_by == "city") {
    $(".filter_by_city").show();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_client").hide();
    $(".filter_by_state").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    $(".clear").show();
    $(".filter").hide();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/branches/cities.php",
      dataType: "json"
    }).done(function (data) {
      $('.city').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        var select= ''
        if (element.id == Number(val)) {
          select = 'selected'
        }
        if(element.city !== null){
          $('.city').append('<option value="'+element.city+'" '+ select +' >'+element.city+'</option>');
        }
      });
    });
  }
  if (filter_by == "customer_for_client") {

    //limpia el select para tomar nuevos datos
    $(".option_remove").remove()

    $(".filter_by_client_for_customer").show();
    $(".filter_by_custom").hide();
    $(".filter_by_city").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_client").hide();
    $(".filter_by_state").hide();
    $(".clear").show();
    $(".filter").hide();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      $('.client_for_customer_id').html('<option value="" selected>Select One</option>');
      data.forEach(element => {
        var select= ''
        if (element.id == Number(val)) {
          select = 'selected'
        }
        $('.client_for_customer_id').append('<option value="' + element.id + '" class="option_remove" '+ select +' >' + element.name + '</option>');
      });
    });
  }
  if(filter_by == "") {
    $(".filter_by_client").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_custom").hide();
    $(".clear").hide();
    $(".filter").hide();
  }
}

function showButton(){
  $(".filter").show();
}

function branches_filter( filtr, val,pag,srch) {
  $(".loading").show()
  var valor = ''
  var filter_by
  if (filtr != null) {
    filter_by = filtr
  }else{
    filter_by = $(".filter_by").val();
  }
 
  var filter_by_custom = $("#filter_custom").val();
  var client_id = $(".client_id").val();
  var cleaner_id = $(".cleaner_id").val();
  var supervisor_id = $(".supervisor_id").val();
  var state = $(".state").val();
  var city = $(".city").val();
  var customer_id = $(".customer_id").val();
  var customer_for_client_id = $(".client_for_customer_id").val();
  
  var page

  if (pag != null) {
    page = pag
  }else{
    page = document.getElementsByClassName("current_page")[0].value;
  }
  console.log(page)
  if (filter_by == "address") {
    var searchstr = "filter=true&filter_by="+filter_by+"&address="+filter_by_custom+"&page="+page;
    valor= filter_by_custom
  }
  if (filter_by == "site_name") {
    var searchstr = "filter=true&filter_by="+filter_by+"&site_name="+filter_by_custom+"&page="+page;
    valor= filter_by_custom
  }
  if (filter_by == "site_code") {
    var searchstr = "filter=true&filter_by="+filter_by+"&site_code="+filter_by_custom+"&page="+page;
    valor= filter_by_custom
  }
  if(filter_by == "client") {
    var searchstr = "filter=true&filter_by="+filter_by+"&client_id="+client_id+"&page="+page;
    valor= client_id
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "customer") {
    var searchstr = "filter=true&filter_by="+filter_by+"&customer_id="+customer_id+"&page="+page;
    valor= customer_id
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "customer_for_client") {
    var searchstr = "filter=true&filter_by="+filter_by+"&customer_id="+customer_for_client_id+"&page="+page;
    valor= customer_for_client_id
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "cleaner") {
    var searchstr = "filter=true&filter_by="+filter_by+"&cleaner_id="+cleaner_id+"&page="+page;
    valor= cleaner_id
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "supervisor") {
    var searchstr = "filter=true&filter_by="+filter_by+"&supervisor_id="+supervisor_id+"&page="+page;
    valor= supervisor_id
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "state") {
    var searchstr = "filter=true&filter_by="+filter_by+"&state="+state+"&page="+page;
    valor= state
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "city") {
    var searchstr = "filter=true&filter_by="+filter_by+"&city="+city+"&page="+page;
    valor= city
    $(".searchstr").val(searchstr);
  }
  console.log(filter_by);
  console.log(valor);
  if (val != null) {
    valor = val
  }
  if (srch != null) {
    searchstr = srch
  }
  console.log(valor);
  window.localStorage.setItem('filtro_sites', JSON.stringify(searchstr));

  window.localStorage.setItem('filter_by', JSON.stringify(filter_by));
  window.localStorage.setItem('value', JSON.stringify(valor));

  $.ajax({
    type: "POST",
    url: "../../../router/branches/index.php",
    data: JSON.stringify({
      page: page,
      searchstr: encodeURI(searchstr),
      filter_by: filter_by,
      value: valor
    }),
    dataType: "html"
  }).done(function (data) {
    $(".loading").hide()
    $("#m-portlet__body").html(data);
  });
}

function search() {
  // Declare variables
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("filter_custom");
  filter = input.value.toUpperCase();
  table = document.getElementById("table_sites");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {

    td_code = tr[i].getElementsByTagName("td")[0];
    td_name = tr[i].getElementsByTagName("td")[1];
    td_address = tr[i].getElementsByTagName("td")[4];

    if (td_code || td_name || td_address) {
      txtValue_code = td_code.textContent || td_code.innerText;
      txtValue_name = td_name.textContent || td_name.innerText;
      txtValue_address = td_address.textContent || td_address.innerText;
      if (txtValue_code.toUpperCase().indexOf(filter) > -1 || txtValue_name.toUpperCase().indexOf(filter) > -1 || txtValue_address.toUpperCase().indexOf(filter) > -1 ) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}

