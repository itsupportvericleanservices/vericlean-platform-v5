function loadFilter(id) {
    $('.loading').show();
  
    var filter = document.getElementById('filter');
    if (filter && filter.value != '') {
      filter = filter.value;
    } else {
      filter = "today";
    }
    console.log(JSON.stringify({
      filter_by: filter,
      id:id
    }));
    $.ajax({
      type: "POST",
      url: "../../../router/route_schedules/filter.php",
      data: JSON.stringify({
        filter_by: filter,
        id:id
      }),
      dataType: "html"
    }).done(function (data) {
      $("#content_filter").html(data);
      $('.loading').hide();
  
    });
  
  }
  
  function loadFilterRange(event) {
    $('.loading').show();
    event.preventDefault()
  
    var start = document.getElementById('range_start').value
    var end = document.getElementById('range_end').value
    var range_start = start
    var range_end = end
  
    $.ajax({
      type: "POST",
      url: "/router/dashboard/filter.php",
      data: JSON.stringify({
        filter_by: 'range',
        range_start: range_start,
        range_end: range_end
      }),
      dataType: "html"
    }).done(function (data) {
      $("#full_data").html(data);
      $('.loading').hide();
  
    });
  
  }
  