var page = 1

function postDirReports(where, page) {
  var per_page = 100
  var fil
  var obj = {}
  if (where == 'home') {
    var today = moment().format('DD-MM-YYYY');
    obj = {
      start: today,
      finished: today,
      client_id: null,
      customer_id: null,
      site: null,
      address: null,
      cleaner_id: null,
      supervisor_id: null,
      state: null,
      city: null,
      status: null,
      full: today,
      page: page,
      per_page: per_page,
      mobile: false,
      first_request: true
    }
  } else if (where == 'form') {
    var client = $('.client_id_form').val()
    var customer = $('.customer_id_form').val()
    var site = $('.site_form').val()
    var address = $('.address_form').val()
    var cleaner = $('.cleaner_id_form').val()
    var supervisor = $('.sup_id_form').val()
    var state = $('.state_id_form').val()
    var city = $('.city_id_form').val()
    var status = $('.status_id_form').val()
    var filtro = $('#from_date_filter_form').val()
    fil = $('#from_date_filter_form').val()
    var m_1 = filtro.substr(0, 2);
    var d_1 = filtro.substr(3, 2);
    var y_1 = filtro.substr(6, 4);

    var m_2 = filtro.substr(13, 2);
    var d_2 = filtro.substr(16, 2);
    var y_2 = filtro.substr(19, 4);

    filtro = `${d_1}-${m_1}-${y_1} - ${d_2}-${m_2}-${y_2}`;
    obj = {
      start: filtro.substr(0, 10),
      finished: filtro.substr(13, 10),
      client_id: client,
      customer_id: customer,
      site: site,
      address: address,
      cleaner_id: cleaner,
      supervisor_id: supervisor,
      state: state,
      city: city,
      status: status,
      full: filtro,
      page: page,
      per_page: per_page,
      mobile: false,
      first_request: false
    }
  }

  $(".loading").show()
  $.ajax({
    type: "POST",
    url: "../../../router/dir_reports/index.php",
    data: JSON.stringify(obj),
    dataType: "html",
  }).done(function (data) {

    $(".loading").hide()

    $("#m-portlet__body").html(data);

    if (fil != null) {
      $("#from_date_filter_form").val(fil);
    }

    if (obj.first_request) {
      $('#myModal').modal({
        backdrop: 'static',
        keyboard: false
      })
      $('#myModal').modal('show')

      $('#myModal').on('hidden.bs.modal', function (e) {
        postDirReports('modal')
      })
    }
  });
}

