function showList() {
  var ls = $(".select_ls").val();
  var by_ls = $(".select_ls_form").val();
  if (ls == "cleaner") {
    $(".sec_sup").hide();
    $(".sec_cle").show();
  } else if (ls == "supervisor") {
    $(".sec_sup").show();
    $(".sec_cle").hide();
  }

  if (by_ls == "cleaner") {
    $(".select_cleaner_form").show();
    $(".select_supervisor_form").hide();
  } else if (by_ls == "supervisor") {
    $(".select_cleaner_form").hide();
    $(".select_supervisor_form").show();
  }
}

function getClrChecks(where) {
  // var filtro = $("#from_date_filter").val();
  // console.log(filtro_m);
  var fil
  var obj = {};
  if (where == "home") {
    var today = moment().format("DD-MM-YYYY");
    obj = {
      start: today,
      finished: today,
      id: 1,
      branch: 0,
      by: "cleaner"
    };
  } else if (where == "modal") {
    var by = $(".select_ls").val();
    var filtro = $("#from_date_filter").val();
    fil = $("#from_date_filter").val();
    var id = 0;
    if (by == "cleaner") {
      id = $(".cleaner_id").val();
    } else if (by == "supervisor") {
      id = $(".sup_id").val();
    }
    var branch = $(".site_id").val();
    var m_1 = filtro.substr(0, 2);
    var d_1 = filtro.substr(3, 2);
    var y_1 = filtro.substr(6, 4);

    var m_2 = filtro.substr(13, 2);
    var d_2 = filtro.substr(16, 2);
    var y_2 = filtro.substr(19, 4);

    filtro = `${d_1}-${m_1}-${y_1} - ${d_2}-${m_2}-${y_2}`;

    obj = {
      start: filtro.substr(0, 10),
      finished: filtro.substr(13, 10),
      id: id,
      branch: branch,
      by: by
    };
    console.log(obj);
  } else if (where == "form") {
    var filtro = $("#from_date_filter_form").val();
    fil = $("#from_date_filter_form").val();
    var id = 0;
    var branch = $(".site_id_form").val();
    var by = $(".select_ls_form").val();
    if (by == "cleaner") {
      id = $(".cleaner_id_form").val();
    } else if (by == "supervisor") {
      id = $(".sup_id_form").val();
    }

    var m_1 = filtro.substr(0, 2);
    var d_1 = filtro.substr(3, 2);
    var y_1 = filtro.substr(6, 4);

    var m_2 = filtro.substr(13, 2);
    var d_2 = filtro.substr(16, 2);
    var y_2 = filtro.substr(19, 4);

    filtro = `${d_1}-${m_1}-${y_1} - ${d_2}-${m_2}-${y_2}`;

    obj = {
      start: filtro.substr(0, 10),
      finished: filtro.substr(13, 10),
      id: id,
      branch: branch,
      by: by
    };
  }

  $(".loading").show();
  console.log(obj);
  $.ajax({
    type: "POST",
    url: "../../../router/dur_checks/index.php",
    data: JSON.stringify(obj),
    dataType: "html"
  }).done(function(data) {
    console.log(obj);
    $(".loading").hide();

    $("#m-portlet__body").html(data);
    if (fil != null) {
      $("#from_date_filter_form").val(fil);
    }
    if (obj.by == "cleaner") {
      $(".select_cleaner_form").show();
      $(".select_supervisor_form").hide();
    } else if (obj.by == "supervisor") {
      $(".select_cleaner_form").hide();
      $(".select_supervisor_form").show();
    }
    showSites("home");
    if (obj.branch == 0) {
      $("#myModal").modal({
        backdrop: "static",
        keyboard: false
      });
      $("#myModal").modal("show");

      $("#myModal").on("hidden.bs.modal", function(e) {
        getClrChecks("modal");
        
        
      });

      $(".sec_sup").hide();
      $(".sec_cle").show();
    }
  });
}
