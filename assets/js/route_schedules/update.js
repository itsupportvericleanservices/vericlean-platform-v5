function updateRouteSchedule(event, id) {
    event.preventDefault();
  
    var route_id = $("#route_id").val();
    var user_id = $("#user_id").val();
    var started_at = $("#started_at").val();
    var finished_at = $("#finished_at").val();
  
    $.ajax({
      type: "POST",
      url: "../../../router/route_schedules/update.php?id=" + id,
      data: JSON.stringify({
        route_id: route_id,
        user_id: user_id,
        started_at: started_at,
        finished_at: finished_at
      }),
      dataType: "json"
    }).done(function (data) {
      swal({
        title: "Success!",
        text: "Route Schedule updated",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
        window.location.href = "../../../route_schedules/index.php";
      });
    }).error(function (err) {
      swal({
        title: 'Error!',
        text: 'Something happened!',
        type: 'error',
        confirmButtonText: 'Cool'
      })
    });
  }