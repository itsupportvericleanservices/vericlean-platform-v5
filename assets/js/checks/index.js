
function getBranches(page) {
  $(".loading").show()
   var from = $('#from_date_filter').val();
  var to = $('#to_date_filter').val();
  $.ajax({
    type: "POST",
    url: "../../../router/checks/index.php",
    data: JSON.stringify({
      page: page,
      from: from,
      to: to,
      print: 0
    }),
    dataType: "html"
  }).done(function (data) {
    $(".loading").hide()
    console.log(data);
    $("#m-portlet__body").html(data);
  });

}

function branches_filter_by() {
  var filter_by = $(".filter_by").val();

  if(filter_by == "checking_by") {
    
    $(".filter_by_checking_by").show();
    $(".filter_by_client").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").hide();
    $(".filter").show();

       
  }
  if(filter_by == "client") {
    $(".option_remove").remove()

    $(".filter_by_client").show();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_checking_by").hide();
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json",
    }).done(function (data) {
      data.forEach(element => {
        $('.client_id').append('<option value="' + element.id + '"class="option_remove" >' + element.name + '</option>');
      });
    });   
  }
  if(filter_by == "customer") {
    $(".option_remove").remove()
    
    $(".filter_by_customer").show();
    $(".filter_by_client").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_checking_by").hide();
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/indirect_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('.customer_id').append('<option value="' + element.id + '" class="option_remove">' + element.name + '</option>');
      });
    });
  }
  if(filter_by == "cleaner") {
    $(".filter_by_cleaner").show();
    $(".filter_by_customer").hide();
    $(".filter_by_client").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_checking_by").hide();
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/cleaners/index.php",
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('.cleaner_id').append('<option value="'+element.id+'">'+element.first_name+' '+element.last_name+'</option>');
      });    
    });  
  }
  if(filter_by == "supervisor" || filter_by == "supervisor_sites" ) {
    $(".filter_by_cleaner").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_client").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").show();
    $(".filter_by_checking_by").hide();
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/supervisors/index.php",
      dataType: "json"
    }).done(function (data) {
      sup = data.sort(function (a, b) {
        if (a.first_name > b.first_name) {
          return 1;
        }
        if (a.first_name < b.first_name) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
      sup.forEach(element => {
        $('.supervisor_id').append('<option value="'+element.id+'">'+element.first_name+' '+element.last_name+'</option>');
      });    
    });  
  }
  if(filter_by == "state") {
    $(".filter_by_state").show();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_client").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_checking_by").hide();
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/branches/states.php",
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        if(element.state !== null){
          $('.state').append('<option value="'+element.state+'">'+element.state+'</option>');
        }
      });    
    });  
  }
  if(filter_by == "city") {
    $(".filter_by_city").show();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_client").hide();
    $(".filter_by_state").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_checking_by").hide();
    $(".filter").show();

    //enviar ajax para llenar el select
    $.ajax({
      type: "POST",
      url: "../../../router/branches/cities.php",
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        if(element.city !== null){
          $('.city').append('<option value="'+element.city+'">'+element.city+'</option>');
        }
      });    
    });  
  }
  if (filter_by == "customer_for_client") {

    //limpia el select para tomar nuevos datos
    $(".option_remove").remove()

    $(".filter_by_client_for_customer").show();
    $(".filter_by_supervisor").hide();
    $(".filter_by_city").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_client").hide();
    $(".filter_by_state").hide();
    $(".filter_by_checking_by").hide();
    $(".filter").show();

    $.ajax({
      type: "POST",
      url: "../../../router/clients/direct_clients_for_status.php",
      data: JSON.stringify({
        status: 1 //active
      }),
      dataType: "json"
    }).done(function (data) {
      data.forEach(element => {
        $('.client_for_customer_id').append('<option value="' + element.id + '" class="option_remove">' + element.name + '</option>');
      });
    });
  }
  if(filter_by == "") {
    $(".filter_by_client").hide();
    $(".filter_by_customer").hide();
    $(".filter_by_cleaner").hide();
    $(".filter_by_state").hide();
    $(".filter_by_city").hide();
    $(".filter_by_client_for_customer").hide();
    $(".filter_by_supervisor").hide();
    $(".filter_by_checking_by").hide();
    $(".filter").show();
  }
}

function check_filter(print = false) {
  var filter_by = $(".filter_by").val();
  var client_id = $(".client_id").val();
  var cleaner_id = $(".cleaner_id").val();
  var supervisor_id = $(".supervisor_id").val();
  var filter_by_checking_by_type = $(".filter_by_checking_by_type").val();
  var state = $(".state").val();
  var city = $(".city").val();
  var customer_id = $(".customer_id").val();
  var customer_for_client_id = $(".client_for_customer_id").val();
  var from = $('#from_date_filter').val();
  var to = $('#to_date_filter').val();

  var page = ($(".current_page").val()) ? $(".current_page").val() : 1;

  switch(filter_by) {
    case "client":
      filter_data = client_id;
      break;
    case "customer":
      filter_data = customer_id;
      break;
    default:
      filter_data = customer_for_client_id;
  }
  
  if(filter_by == "client" || filter_by == "customer" || filter_by == "customer_for_client") {

    var searchstr = "filter=true&filter_by="+filter_by+"&filter_data="+filter_data+"&page="+page;
    $(".searchstr").val(searchstr);

  }
  // if(filter_by == "customer") {
  //   var searchstr = "filter=true&filter_by="+filter_by+"&customer_id="+customer_id+"&page="+page;
  //   $(".searchstr").val(searchstr);
  // }
  // if(filter_by == "customer_for_client") {
  //   var searchstr = "filter=true&filter_by="+filter_by+"&customer_id="+customer_for_client_id+"&page="+page;
  //   $(".searchstr").val(searchstr);
  // }
  if(filter_by == "cleaner") {
    var searchstr = "filter=true&filter_by="+filter_by+"&cleaner_id="+cleaner_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "state") {
    var searchstr = "filter=true&filter_by="+filter_by+"&state="+state+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "city") {
    var searchstr = "filter=true&filter_by="+filter_by+"&city="+city+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "supervisor_sites") {
    var searchstr = "filter=true&filter_by="+filter_by+"&supervisor_id="+supervisor_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "supervisor") {
    var searchstr = "filter=true&filter_by="+filter_by+"&supervisor_id="+supervisor_id+"&page="+page;
    $(".searchstr").val(searchstr);
  }
  if(filter_by == "checking_by") {
    var searchstr = "filter=true&filter_by="+filter_by+"&checking_by="+filter_by_checking_by_type+"&page="+page;
    $(".searchstr").val(searchstr);
  }

  $.ajax({
    type: "POST",
    url: "../../../router/checks/index.php",
    data: JSON.stringify({
      page: page,
      from: from,
      to: to,
      searchstr: encodeURI(searchstr),
      print: print
    }),
    dataType: "html"
  }).done(function (data) {
    $("#m-portlet__body").html(data);
    if (print) {
      javascript:window.print();
    }
  });
}
function paginate_checks(event, action) {
  event.preventDefault();
  $(".loading").show()
  var searchstr = $(".searchstr").val();

  var dataView = parseInt($("#page").val())
  console.log(typeof dataView)
  var from = $('#from_date_filter').val();
  var to = $('#to_date_filter').val();
  if (action == 'next') {
    var pageData = dataView + 1
    $.ajax({
      type: "POST",
      url: "../../../router/checks/index.php",
      data: JSON.stringify({
        page: pageData,
        from: from,
        to: to,
        searchstr: searchstr
      }),
      dataType: "html"
    }).done(function (data) {
      $("#m-portlet__body").html(data);
      $(".loading").hide()
    });
  }
  if (action == 'back') {
    if (dataView > 1) {
      var pageData = dataView - 1
      $.ajax({
        type: "POST",
        url: "../../../router/checks/index.php",
        data: JSON.stringify({
          page: pageData,
          from: from,
          to: to,
          searchstr: searchstr
        }),
        dataType: "html"
      }).done(function (data) {
        $(".loading").hide()
        $("#m-portlet__body").html(data);
      });
    }
  }
}
