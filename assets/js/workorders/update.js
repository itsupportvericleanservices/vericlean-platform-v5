function addAttachment(type) {
  html = '<div class="'+type+'-container">'+
            '<hr style="border-top: dotted 1px;width:90%;align:center;" />'+
            '<div class="row">'+
              '<div class="col-md-6">'+
                '<input type="text" class="form-control m-input attachment_name" placeholder="Enter an optional custom name...">'+
              '</div>'+
              '<div class="col-md-4">'+
                '<input type="file" class="custom-file-input attachments" data-category="'+type+'" onchange="b64FilenameFix(event, this);">'+
                '<label class="custom-file-label" for="'+type+'s">Choose one...</label>'+
              '</div>'+
              '<div class="col-md-2 text-right">'+
                '<button class="btn btn-danger" onclick="removeFileInput(event, this);">Cancel</button>'+
              '</div>'+
            '</div>'+
          '</div>';
  $("."+type+"s-container").append(html);
}

function b64FilenameFix(event, elm) {
  event.preventDefault();

  if($(elm)[0].files.length == 0) {
    file_name = "Choose One..."
  } else {
    file_name = $(elm)[0].files[0].name;
  }

  $(elm).parent().find('.custom-file-label').html(file_name);
}

function removeFileInput(event, elm) {
  event.preventDefault();
  $(elm).parent().parent().parent().hide('slow', function() {
    $(this).remove();
  });
}

documents_arr = [];
invoices_arr = [];
photos_arr = [];
async function b64Files(event, workorder_id) {
  event.preventDefault();
  $('.loading').show();
  attachments_count = 0;
  attachments_total = $('.attachments').length;
  if(attachments_total == 0) {
    updateWorkoder(event, workorder_id);
    return;
  }
  $('.attachments').each(function() {
    category = $(this).data('category');
    custom_name = $(this).parent().parent().find('.attachment_name').val();

    var files = $(this)[0].files;
    if(files.length > 0) {
      for (var i = 0; i < files.length; i++) {
        // Persistent category FIX
        var category = category;
        var file = files[i];
        if (file.type == '') {
          content_type = 'application/'+file.name.split('.').pop();
        } else {
          content_type = file.type;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
          if (category == "document") {
            files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': custom_name}
            documents_arr.push(files_arr_aux);
          }
          else if (category == "invoice") {
            files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': custom_name}
            invoices_arr.push(files_arr_aux);
          } else {
            files_arr_aux = {'file_name': file.name, 'file': reader.result, 'content_type': content_type, 'custom_name': custom_name}
            photos_arr.push(files_arr_aux);
          }
          attachments_count++;
          if(attachments_count >= attachments_total) {
            updateWorkoder(event, workorder_id);
          }
        };
        reader.onerror = function (error) {
          return JSON.stringify({"response": "error", "error": error});
        };
      }
    } else {
      attachments_total--;
    }

    // Create Work Order
    if(attachments_count >= attachments_total) {
      updateWorkoder(event, workorder_id);
    }
  });
}

function updateWorkoder(event, workorder_id) {
  event.preventDefault();

  var internal_id = $("#internal_id").val();
  var user_id = $("#user_id").val();
  var branch_id = $("#branch_id").val();
  var cleaner_id = $("#cleaner_id").val();
  var requested_date = $("#requested_date").val();
  var due_date = $("#due_date").val();
  var site_contact = $("#site_contact").val();
  var request_contact_email = $("#request_contact_email").val();
  var site_phone = $("#site_phone").val();
  var request_contact = $("#request_contact").val();
  var request_phone = $("#request_phone").val();
  var task_name = $("#task_name").val();
  var task_description = $("#task_description").val();
  var status = $("#status").val();
  var type = $("#type").val();
  var priority = $("#priority").val();
  var fee = $("#fee").val();
  var date = $("#scheduled_at_date").val();
  var time = $("#scheduled_at_time").val();
  var scheduled_at = `${date} ${time}`
  var comments = $("#comments").val();

  var client_id = $("#client_id").val();
  var customer_id = $("#customer_id").val();
  var type_client_id = 0

  var type_client = $("#type_client").val();
  if(type_client == "client"){
    type_client_id = client_id
  }
  if(type_client == "customer"){
    type_client_id = customer_id
  }


  //var documents = $("#document").val() - ver como hacer aqui
  json = JSON.stringify({
    internal_id: internal_id,
    user_id: user_id,
    branch_id: branch_id,
    client_id: type_client_id,
    cleaner_id: cleaner_id,
    requested_date: requested_date,
    due_date: due_date,
    site_contact: site_contact,
    request_contact_email: request_contact_email,
    site_phone: site_phone,
    request_contact: request_contact,
    request_phone: request_phone,
    task_name: task_name,
    task_description: task_description,
    status: status,
    main_type: type,
    priority: priority,
    fee: fee,
    scheduled_date: scheduled_at,
    comments: comments,
    documents: documents_arr,
    invoices: invoices_arr,
    photos: photos_arr
  });


  $.ajax({
    type: "POST",
    url: "../../../router/workorders/update.php?id=" + workorder_id,
    data: json,
    dataType: "json"
  }).done(function (data) {
    $('.loading').hide();
    // if(data.id > 0) {
      swal({
        title: "Success!",
        text: "Workorder updated",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(result => {
        window.location.href = "../../../workorders/index.php";
      });
    // } else {
    //   swalError();
    // }
  }).fail(function (err) {
    $('.loading').hide();
    swalError();
  });
}


function verifydispatched() {
  var status = $("#status").val();
  var actual_status = $("#actual_status").val();

  if (actual_status == 3 && status == 4) {
    $("#status").val(5);
  }
  if (actual_status == 4 && status == 3) {
    $("#status").val(5);
  }
}


function selectClient(){

  var type_client = $("#type_client").val();

  if(type_client == "client"){
    $("#client_content").show();
    $("#customer_content").hide();
  }else{
    $("#client_content").hide();
    $("#customer_content").show();
  }

}

function sitesForClient(){
  console.log("entrio");
  var client_id = $("#client_id").val();
  var customer_id = $("#customer_id").val();
  $("#branch_id_container").show();

  var type = (client_id != '') ? "client" : "customer";
  client_id = (client_id != '') ? client_id : customer_id;

  $.ajax({
    type: "POST",
    url: "../../../router/clients/sites_for_client_or_customer.php",
    data: JSON.stringify({
      type: type,
      client_id: client_id
    }),
    dataType: "json",
  }).done(function (data) {
    $('#branch_id').html('<option value="" selected>Select One</option>');
    data.forEach(element => {
      var name_desc = '';
      name_desc += (element.site_code) ? element.site_code : '';
      name_desc += (element.name) ? ' - '+element.name : '';
      name_desc += (element.address) ? ' - '+element.address : '';
      $('#branch_id').append('<option value="' + element.id + '" class="option_remove">' + name_desc + '</option>');

    });
  });

}
