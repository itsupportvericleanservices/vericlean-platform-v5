function createBuildingType(event) {
  event.preventDefault();

  var name = $("#name").val();

  let datos = ['name'];

  let inputs = document.getElementsByTagName('input')
  let inp = [...inputs]
  let filtrados = datos.map(el => {

      let input = inp.find(inp=>{
        return inp.id ==  el
      })
      if (input.value == "") {
        input.classList.add("is-invalid")
      }else if(input.classList.contains("is-invalid")){
        input.classList.remove("is-invalid")
      }
      return input
  })

  let verifica = (arr, fn) => arr.every(fn);
  let stat = verifica(filtrados,x => x.value != "");

  if (stat) {
    $.ajax({
      type: "POST",
      url: "../../../router/building_types/create.php",
      data: JSON.stringify({
        name: name
      }),
      dataType: "json"
    }).done(function() {
      swal({
        title: "Success!",
        text: "Building Type created",
        icon: "success",
        button: "Ok",
        closeOnEsc: true
      }).then(() => {
        window.location.href = "../../../building_types/index.php";
      });
    }).error(function() {
      swal({
        title: 'Error!',
        text: 'Something happened!',
        type: 'error',
        confirmButtonText: 'Cool'
      })
    });
  } else {
    swal({
      title: 'Error!',
      text: 'Verify you data is complete!',
      type: 'error',
      confirmButtonText: 'ok'
    });
  }
}