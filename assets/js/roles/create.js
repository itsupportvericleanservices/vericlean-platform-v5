function createRole(event) {
  event.preventDefault();

  var name = $("#name").val();

  $.ajax({
    type: "POST",
    url: "../../../router/roles/create.php",
    data: JSON.stringify({
      name: name,
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Role created",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../roles/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}