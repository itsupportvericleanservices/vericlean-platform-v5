function updateRole(event, id) {
  event.preventDefault();

  var name = $("#name").val();

  $.ajax({
    type: "POST",
    url: "../../../router/roles/update.php?id=" + id,
    data: JSON.stringify({
      name: name,
    }),
    dataType: "json"
  }).done(function (data) {
    swal({
      title: "Success!",
      text: "Role updated",
      icon: "success",
      button: "Ok",
      closeOnEsc: true
    }).then(result => {
      window.location.href = "../../../roles/index.php";
    });
  }).error(function (err) {
    swal({
      title: 'Error!',
      text: 'Something happened!',
      type: 'error',
      confirmButtonText: 'Cool'
    })
  });
}