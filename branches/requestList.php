<?php
include('../includes/header.php');
$requests = vcGetBranchesListRequest($_SESSION['access-token']);
?>
<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
 
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">List of request for update location</h3>
        </div>
      </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
      <!--Begin::Section-->
      <div class="row">
        <div class="col-xl-12">

          <!--begin::Portlet-->
          <div class="m-portlet">

            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    Options
                  </h3>
                </div>
              </div>
              <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                    <!-- <a href="create.php" class="btn btn-primary">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>New Service Type</span>
                                    </span>
                                </a> -->
                  </li>
                </ul>
              </div>
            </div>
            
            <div class="m-portlet__body">
              <!--begin::Section-->
              <div class="m-section">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table table-bordered">
                    <thead>
                      <tr>
                      <th>ID</th>
                      <th width="70px">Site Code</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Client</th>
                      <th>Request User</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Zipcode</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                                        foreach ($requests as $key => $serv) {
                                    ?>
                      <tr>
                      <td scope="row">
                        <?= $serv['id'] ?>
                      </td>
                      <td scope="row">
                        <p >
                          <?= $serv['site_code'] ?>
                        </p>
                          
                      </td>
                      <td>
                        <p>
                          <?php echo $serv['name'] ?>
                        </p>
                      </td>
                      <td>
                        <?php if($serv['client_indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
                      </td>
                      <td>
                        <?= $serv['client_name'] ?>
                      </td>
                      <td>
                      <?= $serv['users_name'] ?>

                      </td>
                      <td>
                        <p>
                          <?php echo $serv['address'] ?>
                        </p>
                      </td>
                      <td>
                        <p>
                          <?php echo $serv['city'] ?>
                        </p>
                      </td>
                      <td>
                        <p>
                          <?php echo $serv['state'] ?>
                        </p>
                      </td>
                      <td>
                        <p>
                          <?php echo $serv['zipcode'] ?>
                        </p>
                      </td>
                        <!-- <td scope="row">
                          <a href="show.php?id=<?= $serv['id'] ?>">
                            <?= $serv['id'] ?>
                          </a>
                        </td>
                        <td>
                          <a href="show.php?id=<?= $serv['id'] ?>">
                            <?= $serv['name'] ?>
                          </a>
                        </td> -->
                        <td class="noPrint">
                          <span class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                              data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item" onclick="showMap(<?= $serv['latitude'] ?>,<?= $serv['longitude'] ?>,<?= $serv['update_location_lat'] ?>,<?= $serv['update_location_lng'] ?>)"><i
                                  class="la la-crosshairs"></i>
                                view map</a>
                              <a class="dropdown-item" onclick="updateBranchLocation(event, <?= $serv['id'] ?>, <?= $serv['update_location_lat'] ?>, <?= $serv['update_location_lng'] ?>)"><i
                                  class="la la-edit"></i>
                                Confrm</a>
                              <a class="dropdown-item" onclick="updateBranchLocationReject(event, <?= $serv['id'] ?>)"><i
                                  class="fas fa-minus-circle"></i>
                                Reject</a>
                              <!-- onclick="showMap('17.0542297','-96.7132304','17.0542297','-96.7132304')" -->

                            </div>
                          </span>
                        </td>
                        <!-- <td>
                                            <span class="dropdown">
                                                <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                                    data-toggle="dropdown" aria-expanded="true">
                                                    <i class="la la-ellipsis-h"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="show.php?id=<?= $serv['id'] ?>"><i class="la la-crosshairs"></i>
                                                        View</a>
                                                    <a class="dropdown-item" href="update.php?id=<?= $serv['id'] ?>"><i class="la la-edit"></i>
                                                        Update</a>
                                                    <a onclick="deleteServiceType(event, <?= $serv['id'] ?>)" class="dropdown-item"><i class="la la-trash"></i>
                                                        Delete</a>
                                                </div>
                                            </span>
                                        </td> -->
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>

              <!--end::Section-->
            </div>

            <!--end::Form-->
          </div>
          <!--end::Portlet-->
          <!--End::Section-->
        </div>
      </div>
      <!--END XL12-->
    </div>
    <!--END ROW-->
  </div>

  <input type="hidden" class="searchstr" name="searchstr" value="" />
  <!-- Modal -->
  <div class="modal fade" id="check_map" tabindex="-1" role="dialog" aria-labelledby="check_mapModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <p style="margin-right:20px;"><i><img src="../assets/images/icons/blue_marker.png" alt="" style="width: 30px;"></i>Stored Location</p>
        <p><i><img src="../assets/images/icons/red_marker.png" alt="" style="width: 30px;"></i>Request Location</p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <br/>
          
        </div>
        <div class="modal-body">
          <div id="map" style="width: 100%; height: 500px"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>



  <!--begin::CHECKS -->
  <!-- <script src="../assets/js/checks/index.js" type="text/javascript"></script> -->
  <!--end::CHECKS -->
  <script src="../assets/js/branches/update.js"></script>
  <?php include('../includes/footer.php');?>

  <script>
    $(document).ready(function () {
      document.addEventListener('DOMContentLoaded', getBranches(1));
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>
  <script>
    function showMap(lat_in, lng_in, lat_out, lng_out) {
      clearMarkers()
      map.setZoom(7);
        var location = new google.maps.LatLng(lat_out, lng_out);
        var location_now = new google.maps.LatLng(lat_in, lng_in);
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        var marker_now = new google.maps.Marker({
          position: location_now,
          map: map,
          icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
          }
        });
        markers.push(marker);
        markers.push(marker_now);
        map.setCenter(location);
        map.setZoom(16);
        setTimeout(function () {
          map.setCenter(location);
          map.setZoom(16);
        }, 500)

      



      $('#check_map').modal('show');
    }

    function setAddressMap(geocoder, map) {
      var address = $("#address").val();
      var address2 = $("#address2").val();
      var zipcode = $("#zipcode").val();
      var city = $("#city").val();
      var state = $("#state").val();
      var address_search = address + ' ' + address2 + ' ' + zipcode + ' ' + city + ' ' + state;
      geocoder.geocode({
        'address': address_search
      }, function (results, status) {
        if (status == 'OK') {
          clearMarkers();
          map.setCenter(results[0].geometry.location);
          map.setZoom(16);
          var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location

          });
          markers.push(marker);
          $('#latitude').val(results[0].geometry.location.lat());
          $('#longitude').val(results[0].geometry.location.lng());
        } else {
          alert('Address not found: ' + status);
        }
      });
    }
    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }
    var map;
    var geocoder;
    var markers = [];

    function initMap() {
      geocoder = new google.maps.Geocoder();
      map = new google.maps.Map(document.getElementById('map'), {
        center: {
          lat: 32.124127,
          lng: -100.4939386
        },
        zoom: 5
      });


    }
  </script>
  <!--end::GET LATITUDE AND LONGITUDE OF GOOGLE MAPS -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBo86o_PxdPQqtFTP8_byHVF8ExqWfnqP4&callback=initMap"
    async defer></script>