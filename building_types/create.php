<?php include('../includes/header.php') ?>

<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
  <?php include('../includes/sidebar_menu.php');?>

  <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">New Building Type</h3>
        </div>
      </div>
    </div>

    <div class="m-content">
      <div class="row">
        <div class="col-xl-12">
          <div class="m-portlet">
            <div class="m-portlet__body">
              <form class="m-form m-form--fit m-form--label-align-right">
                <div class="form-group m-form__group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control m-input" id="name" placeholder="Name">
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions">
                    <a onclick="createBuildingType(event)" class="btn btn-primary">Save</a>
                    <a href="index.php" type="reset" class="btn btn-secondary">Cancel</a >
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include('../includes/footer.php');?>

<script src="../assets/js/building_types/create.js" type="text/javascript"></script>
