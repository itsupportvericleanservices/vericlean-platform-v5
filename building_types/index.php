<?php

include('../includes/header.php');

$building_types = vcGetBuildingTypes($_SESSION['access-token']);

?>

<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
  <?php include('../includes/sidebar_menu.php');?>

  <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Building Types</h3>
        </div>
      </div>
    </div>

    <div class="m-content">
      <div class="row">
        <div class="col-xl-12">
          <div class="m-portlet">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    Options
                  </h3>
                </div>
              </div>

              <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                    <a href="create.php" class="btn btn-primary">
                      <span>
                        <i class="la la-plus"></i>
                        <span>New Building Type</span>
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="m-portlet__body">
            <div class="m-section">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($building_types as $building_type) { ?>
                        <tr>
                          <td scope="row">
                            <a href="show.php?id=<?= $building_type['id'] ?>">
                              <?= $building_type['id'] ?>
                            </a>
                          </td>
                          <td>
                            <a href="show.php?id=<?= $building_type['id'] ?>">
                              <?= $building_type['name'] ?>
                            </a>
                          </td>
                          <td>
                            <span class="dropdown">
                              <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                data-toggle="dropdown" aria-expanded="true">
                                <i class="la la-ellipsis-h"></i>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="update.php?id=<?= $building_type['id'] ?>"><i class="la la-edit"></i>
                                  Update</a>
                                <a onclick="deleteBuildingType(event, <?= $building_type['id'] ?>)" class="dropdown-item"><i class="la la-trash"></i>
                                  Delete</a>
                              </div>
                            </span>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="../assets/js/building_types/delete.js" type="text/javascript"></script>

<?php include('../includes/footer.php'); ?>
