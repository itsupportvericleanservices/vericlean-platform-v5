<?php
include('../includes/header.php');
$clients = vcGetClients($_SESSION['access-token']);
$cleaners = vcGetUsers($_SESSION['access-token']); //vcGetCleaners($_SESSION['access-token']);
$active_sites = vcGetActiveBranches($_SESSION['access-token']);

?>


<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">


<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">New Workorder</h3>
            <input type="text" value="<?php echo $_SESSION['id'] ?>" id="user_id" style="display:none">
      
        </div>
    </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            
            <!--begin::Portlet-->
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="create">
                        <div class="m-portlet__body">

                            <div class="form-group m-form__group">
                                <label for="clients">Type of Client</label>
                                <select class="form-control m-input" id="type_client" name="type_client" onchange="selectClient()">
                                    <option value="">Select One</option>
                                    <option value="client" >Client</option>
                                    <option value="customer" >Customer</option>
                                </select>
                            </div>
                            <div class="form-group m-form__group" id="client_content" style="display:none" >
                                <label>Clients</label>
                                <select class="form-control m-input" id="client_id" name="client_id" onchange="sitesForClient()">
                                    <option value="" selected>Select One</option>
                                </select>
                            </div>
                            <div class="form-group m-form__group" id="customer_content" style="display:none" >
                                <label>Customers</label>
                                <select class="form-control m-input" id="customer_id" name="customer_id" onchange="sitesForClient()">
                                    <option value="" selected>Select One</option>
                                </select>
                            </div>
                            <div class="form-group m-form__group" id="branch_id_container" style="display:none">
                                <label>Sites</label>
                                <select class="form-control m-input" id="branch_id" name="branch_id" onchange="$('#div_data_wo').show()" >
                                    <option value="" selected>Select One</option>
                                </select>
                            </div>
                            <div id="div_data_wo" class="hidden">
                                <div class="form-group m-form__group">
                                    <label>Customer Work Order#</label>
                                    <input type="text" class="form-control m-input" id="internal_id" placeholder="Internal ID"  >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Cleaners</label>
                                    
                                    <select class="form-control m-input" id="cleaner_id" name="cleaner_id"  >
                                        <option value="" selected>Select One</option>
                                        <?php  usort($cleaners, function($a, $b) {
                                                return strtolower($a['first_name']) > strtolower($b['first_name']);
                                                
                                                });
                                                
                                         foreach ($cleaners as $key => $cleaner) { ?>
                                            <option value="<?php echo $cleaner['id'] ?>"><?= $cleaner['first_name'].' '.$cleaner['last_name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Request Date</label>
                                    <input type="date" class="form-control m-input" id="requested_date" placeholder="Request Date"  >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Due Date</label>
                                    <input type="date" class="form-control m-input" id="due_date" placeholder="Due date" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Site Contact</label>
                                    <input type="text" class="form-control m-input" id="site_contact" placeholder="Site Contact" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Site Phone</label>
                                    <input type="text" class="form-control m-input" id="site_phone" placeholder="Site Phone" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Request Contact</label>
                                    <input type="text" class="form-control m-input" id="request_contact" placeholder="Request Contact" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Request Contact Email</label>
                                    <input type="text" class="form-control m-input" id="request_contact_email" placeholder="Site Contact email" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Request Phone</label>
                                    <input type="text" class="form-control m-input" id="request_phone" placeholder="Request Phone" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Task Name</label>
                                    <input type="text" class="form-control m-input" id="task_name" placeholder="Task Name" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Task Description</label>
                                    <textarea class="form-control m-input" id="task_description" placeholder="Task Description" rows="4" cols="50" ></textarea  >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Status</label>
                                    <select class="form-control m-input" id="status" name="status" >
                                        <option value="0" selected>Select One</option>
                                        <option value="0">Opened</option>
                                        <option value="1">Quoted</option>
                                        <option value="2">Approved</option>
                                        <option value="3">Scheduled</option>
                                        <option value="4">Assigned</option>
                                        <option value="5">Dispatched</option>
                                        <option value="6">In Progress</option>
                                        <option value="12" <?php if($workorder['status'] == 12){ echo "selected"; } ?>>Ready to verify</option>
                                        <option value="7">Completed</option>
                                        <option value="8">Refused</option>
                                        <option value="9">Rejected</option>
                                        <option value="10">Invoiced</option>
                                        <option value="11">Payed</option>
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Types</label>
                                    <select class="form-control m-input" id="type" name="type" >
                                        <option value="" selected>Select One</option>
                                        <option value="emergency">Emergency</option>
                                        <option value="complain">Complain</option>
                                        <option value="rush_request">Rush Request</option>
                                        <option value="request">Request</option>
                                        <option value="periodics">Periodics</option>
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Priority</label>
                                    <select class="form-control m-input" id="priority" name="priority" >     <option value="" selected>Select One</option>
                                        <option value="1">P1</option>
                                        <option value="2">P2</option>
                                        <option value="3">P3</option>
                                        <option value="4">P4</option>
                                        <option value="5">P5</option>
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Fee</label>
                                    <input type="number" class="form-control m-input" id="fee" placeholder="Fee" >
                                </div>
                                <label>Schedule At</label>
                                <div class="form-group m-form__group row">
                                    
                                    <div class="col-md-6">
                                    <label>Date</label>
                                    <input type="date" class="form-control m-input" id="scheduled_at_date" placeholder="YYYY-MM-DD"  >
                                    </div>
                                    <div class="col-md-6">
                                    <label>Time (in 24 hour format)</label>
                                    <input type="datetime" class="form-control m-input" id="scheduled_at_time" placeholder="23:59"  >
                                    </div>
                                    
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Comments</label>
                                    <textarea class="form-control m-input" id="comments" placeholder="Comments" rows="4" cols="50" ></textarea  >
                                </div>
                                <div class="form-group m-form__group">
                                    <label>Instructions</label>
                                    <textarea class="form-control m-input" id="instructions" placeholder="Instructions" rows="4" cols="50" ></textarea  >
                                </div>

                                <div class="form-group m-form__group form_document_group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Documents</label>
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-success" style="float: right; widht: 25px !important; height: 25px !important; line-height:0" onclick="addAttachment('document');">Add a Document</a>
                                        </div>
                                    </div>
                                    <div class="documents-container">
                                    </div>
                                </div>

                                <hr />
                                <div class="form-group m-form__group form_document_group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Photos</label>
                                        </div>
                                        <div class="col-md-6 text-right">
                                          <a class="btn btn-success" style="height: 25px !important; line-height:0" onclick="addAttachment('photo');">Add a Photo</a>
                                        </div>
                                    </div>
                                    <div class="photos-container">
                                    </div>
                                </div>

                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <a onclick="b64Files(event);" class="btn btn-primary">Save</a>
                                        <a href="index.php" type="reset" class="btn btn-secondary">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
            <!--End::Section-->
        </div>
    </div>
    <!--END XL12-->
</div>
<!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->



<?php include('../includes/footer.php');?>

<!--begin::CLIENTS -->
<script src="../assets/js/plugins/cleave.min.js"></script>
<script src="../assets/js/workorders/create.js" type="text/javascript"></script>
<script>
    $("#cleaner_id").chosen({no_results_text: "Oops, nothing found!"}); 
</script>
<!--end::CLIENTS -->
