<?php
include('../includes/header.php');
$workorder = vcGetWorkorder($_SESSION['access-token'], $_GET['id']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Workorder <?php echo $workorder['internal_id'] ?></h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                        <!--Begin::Section-->
                        <div class="m-portlet">
							<div class="m-portlet__body m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
									<div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
                                                        <span class="m-widget1__title">Customer Work Order#</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['internal_id'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">User</span>
														<h3 class="m-widget1__title m--font-brand"><?= $_SESSION['first_name'] ?></h3>
													</div>
												</div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Site</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['branch']['address'] ?></h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
                                                        <span class="m-widget1__title">Client</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['client']['name'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Cleaner</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['cleaner']['first_name']. " " . $workorder['cleaner']['last_name'] ?></h3>
													</div>
												</div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Request Date</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= ($workorder['requested_date'] != null) ? date('Y-m-d H:i:s', strtotime($workorder['requested_date'])) :'';
															?>

															</h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
                                                        <span class="m-widget1__title">Due Date</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= ($workorder['due_date'] != null) ? date('Y-m-d H:i:s', strtotime($workorder['due_date'])) :'';
															?>
															</h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Site Contact</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['site_contact']?></h3>
													</div>
												</div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Site Phone</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['site_phone']?></h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>


                                     <div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
                                                        <span class="m-widget1__title">Task Name</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['task_name']?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Task Description</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['task_description']?></h3>
													</div>
												</div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Status</span>
														<h3 class="m-widget1__title m--font-brand">
															<?php if($workorder['status'] == 0){ ?>
																<span class="m-badge m-badge--metal m-badge--wide">Opened</span>
															<?php } ?>
															<?php if($workorder['status'] == 1){ ?>
																<span class="m-badge m-badge--metal m-badge--wide">Quoted</span>
															<?php } ?>
															<?php if($workorder['status'] == 2){ ?>
																<span class="m-badge m-badge--metal m-badge--wide">Approved</span>
															<?php } ?>
															<?php if($workorder['status'] == 3){ ?>
																<span class="m-badge m-badge--warning m-badge--wide">Scheduled</span>
															<?php } ?>
															<?php if($workorder['status'] == 4){ ?>
																<span class="m-badge m-badge--metal m-badge--wide">Assigned</span>
															<?php } ?>
															<?php if($workorder['status'] == 5){ ?>
																<span class="m-badge m-badge--metal m-badge--wide">Dispatched</span>
															<?php } ?>
															<?php if($workorder['status'] == 6){ ?>
																<span class="m-badge m-badge--info m-badge--wide">In Progress</span>
															<?php } ?>
															<?php if($workorder['status'] == 7){ ?>
																<span class="m-badge m-badge--success m-badge--wide">Completed</span>
															<?php } ?>
															<?php if($workorder['status'] == 8){ ?>
																<span class="m-badge m-badge--danger m-badge--wide">Refused</span>
															<?php } ?>
															<?php if($workorder['status'] == 9){ ?>
																<span class="m-badge m-badge--danger m-badge--wide">Rejected</span>
															<?php } ?>
															<?php if($workorder['status'] == 10){ ?>
																<span class="m-badge m-badge--danger m-badge--wide" style="background: #9d6363 !important">Invoiced</span>
															<?php } ?>
															<?php if($workorder['status'] == 11){ ?>
																<span class="m-badge m-badge--danger m-badge--wide" style="background: #c14a26 !important">Payed</span>
															<?php } ?>
														</h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>

                                     <div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
                                                        <span class="m-widget1__title">Type</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['main_type']?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Priority</span>
														<h3 class="m-widget1__title m--font-brand">
															<?php if($workorder['priority'] == 1){ ?>
																P1
															<?php } ?>
															<?php if($workorder['priority'] == 2){ ?>
																P2
															<?php } ?>
															<?php if($workorder['priority'] == 3){ ?>
																P3
															<?php } ?>
															<?php if($workorder['priority'] == 4){ ?>
																P4
															<?php } ?>
															<?php if($workorder['priority'] == 5){ ?>
																P5
															<?php } ?>
														</h3>
													</div>
												</div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Fee</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['fee']?></h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
                                                        <span class="m-widget1__title">Schedule At</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= ($workorder['scheduled_date'] != null) ? date('Y-m-d H:i:s', strtotime($workorder['scheduled_date'])) :'';
															?>

															</h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Started At</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= ($workorder['started_date'] != null) ? date('Y-m-d H:i:s', strtotime($workorder['started_date'])) :'';
															?>

															</h3>
													</div>
												</div>

                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Finished At</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= ($workorder['finished_date'] != null) ? date('Y-m-d H:i:s', strtotime($workorder['finished_date'])) :'';
															?>

															</h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div id="map" style="width: 500%; height: 500px"></div>
												</div>
											</div>
											<!-- <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Started Long</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= $workorder['started_lng']?>

														</h3>
													</div>
												</div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Finished Lat</span>
														<h3 class="m-widget1__title m--font-brand"><?= $workorder['finished_lat']?></h3>
													</div>
												</div>
											</div> -->
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>




                                    <div class="col-md-12 col-lg-12 col-xl-4">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">

											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Created At</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= ($workorder['created_at'] != null) ? date('Y-m-d H:i:s', strtotime($workorder['created_at'])) :'';
															?>

															</h3>
													</div>
												</div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Updated At</span>
														<h3 class="m-widget1__title m--font-brand">
															<?= ($workorder['updated_at'] != null) ? date('Y-m-d H:i:s', strtotime($workorder['updated_at'])) :'';
															?>

															</h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                  </div>
                  
                  <div class="col-md-12 col-lg-12 col-xl-12">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
                          	<h4 class="page-header">Documents</h4>
														<table class="table">
															<thead>
																<tr>
																	<th scope="col">#</th>
																	<!-- <th scope="col">Custom Name</th> -->
																	<th scope="col">Filename</th>
																	<th scope="col">Options</th>
																</tr>
															</thead>
															<tbody>
															<?php foreach ($workorder['documents'] as $key => $file) { 

																?>
																<tr>
																	<td></td>
																	<!-- <td><?= $file['attachments'][0]['blob']['filename']; ?></td> -->
																	<td><?= $file['attachments'][0]['blob']['filename']; ?></td>
																	<td>
																		<a href="<?= $file['attachments'][0]['blob']['service_url']; ?>" target="_blank">
																			<i class='flaticon-interface-11'></i> Download
																		</a>
																	</td>
																</tr>
															<?php } ?>
															<tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                  </div>

                  <div class="col-md-12 col-lg-12 col-xl-12">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col-12">
                            <h4 class="page-header">Photos</h4>
														<?php foreach ($workorder['photos'] as $key => $file) { ?>
															<a class="thumbnail" href="<?= $file['attachments'][0]['blob']['service_url']; ?>" data-title="<?= $file['attachments'][0]['blob']['filename']; ?>" data-lightbox="workorder">
																<img class="img-responsive img-thumbnail" src="<?= $file['attachments'][0]['blob']['service_url']; ?>" height="200px" />
															</a>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                  </div>

                  <div class="col-md-12 col-lg-12 col-xl-12">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col-12">
                            <h4 class="page-header">Signature</h4>
														<?php foreach ($workorder['signatures'] as $key => $file) { ?>

															<a class="thumbnail" href="<?= $file['attachments'][0]['blob']['service_url']; ?>" data-title="<?= $file['attachments'][0]['blob']['filename']; ?>" data-lightbox="signature">
																<img class="img-responsive img-thumbnail" src="<?= $file['attachments'][0]['blob']['service_url']; ?>" height="200px" />
															</a>

														<?php } ?>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                  </div>
								</div>
							</div>
						</div>
                    </div>
                </div>
        <!--END XL12-->
     </div>
    <!--END ROW-->
</div>
<!--End::Content-->

<?php include('../includes/footer.php');?>
<!-- end:: Page -->
<script>
    function showMap(){
        clearMarkers()
        map.setZoom(7);
        if (lat_in != '') {
            var location = new google.maps.LatLng(lat_in, lng_in);
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            map.setCenter(location);
            map.setZoom(16);
            setTimeout(function(){
              map.setCenter(location);
              map.setZoom(16);
            },500)

        }
        if (lat_out != '') {
            var location = new google.maps.LatLng(lat_out, lng_out);
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
            setTimeout(function(){
              map.setCenter(location);
              map.setZoom(16);
            },500)
        }


    }
    function setAddressMap(geocoder, map){
      var address = $("#address").val();
      var address2 = $("#address2").val();
      var zipcode = $("#zipcode").val();
      var city = $("#city").val();
      var state = $("#state").val();
      var address_search = address + ' ' + address2 + ' ' + zipcode + ' ' + city + ' ' + state;
        geocoder.geocode( { 'address': address_search}, function(results, status) {
          if (status == 'OK') {
            clearMarkers();
            map.setCenter(results[0].geometry.location);
            map.setZoom(16);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location

            });
            markers.push(marker);
            $('#latitude').val(results[0].geometry.location.lat());
            $('#longitude').val(results[0].geometry.location.lng());
          } else {
            alert('Address not found: ' + status);
          }
        });
    }
    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
    }

      // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }
    var map;
    var geocoder;
    var markers = [];
    function initMap() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 32.124127, lng: -100.4939386},
          zoom: 5
        });
        showMap();


    }
</script>
<!--end::GET LATITUDE AND LONGITUDE OF GOOGLE MAPS -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBo86o_PxdPQqtFTP8_byHVF8ExqWfnqP4&callback=initMap'
async defer></script>
<?php
// var_dump($workorder['started_lat']);
$lat_in = ($workorder['started_lat'] != null) ? $workorder['started_lat'] : "''";
$lng_in = ($workorder['started_lng'] != null) ? $workorder['started_lng'] : "''";
$lat_out = ($workorder['finished_lat'] != null) ? $workorder['finished_lat'] : "''";
$lng_out = ($workorder['finished_lng'] != null) ? $workorder['finished_lng'] : "''";
echo "<script>var lat_in = ".$lat_in."; </script>";
echo "<script>var lng_in = ".$lng_in."; </script>";
echo "<script>var lat_out = ".$lat_out."; </script>";
echo "<script>var lng_out = ".$lng_out."; </script>";

 ?>
