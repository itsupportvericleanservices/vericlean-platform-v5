<?php
include('../includes/header.php');
// $clients = vcGetClients($_SESSION['access-token']);
// $cleaners = vcGetUsers($_SESSION['access-token']); //vcGetCleaners($_SESSION['access-token']);
// $active_sites = vcGetActiveBranches($_SESSION['access-token']);

?>


<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">


<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title ">New Workorder</h3>
            <input type="hidden" value="<?php echo $_SESSION['id'] ?>" id="user_id">
            <input type="hidden" value="" id="branch_id" >
      
        </div>
    </div>
</div>

<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="row">
        <div class="col-xl-12">
            
            <!--begin::Portlet-->
            <div class="m-portlet">

                <div class="m-portlet__body">

                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right" id="create">
                        <div class="m-portlet__body">

                            <div id="" class="">
                                
                                <div class="form-group m-form__group form_document_group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>File</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="custom-file-input attachments" data-category="document" onchange="parseHTMLFile(event, this);">
                                            <label class="custom-file-label" for="documents">Select a file...</label>
                                        </div>
                                    </div>
                                    <div class="documents-container">
                                    </div>
                                </div>

                                
                                <div id="html_wo"></div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <a onclick="saveWoFile(event);" class="btn btn-primary">Save</a>
                                        <a href="index.php" type="reset" class="btn btn-secondary">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
            <!--End::Section-->
        </div>
    </div>
    <!--END XL12-->
</div>
<!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->



<?php include('../includes/footer.php');?>

<!--begin::CLIENTS -->
<script src="../assets/js/plugins/cleave.min.js"></script>
<script src="../assets/js/workorders/create.js" type="text/javascript"></script>
<script>
    $("#cleaner_id").chosen({no_results_text: "Oops, nothing found!"}); 
</script>
<!--end::CLIENTS -->
