<?php include('../includes/functions.php'); ?>
<?php
$lat = $_POST['lat'];
$lng = $_POST['lng'];
if ($_SESSION['role_id']== 6) {
	$sites = vcGetBranchesByCleaner($_SESSION['access-token'], $_SESSION['id'],$lat, $lng);
} else {
	$sites = vcGetBranchesBySupervisor($_SESSION['access-token'], $_SESSION['id'],$lat, $lng);
}
if (count($sites) > 0) {

		foreach ($sites as $key => $branch) {


				date_default_timezone_set('America/Chicago');
				$max_time = explode('.000Z', explode('T', $branch['branch']['max_time'])[1])[0];
				$max_time = strtotime($max_time);
				$now_time = date('H:i:s');
				$now_time = strtotime($now_time);
				//var_dump($max_time);
				//var_dump($now_time);
				$class_branch = '';
				if ($max_time != null) {

						if ( ($now_time - $max_time)/60 > 1 && $branch['histories'][0]['started_at'] == null) {
								$class_branch = 'warning_yellow';
						}
						if(($now_time - $max_time)/60 > 29 && ($now_time - $max_time)/60 < 59 && $branch['histories'][0]['started_at'] == null){
								$class_branch = 'warning_orange';
						}
						if(($now_time - $max_time)/60 > 59 && $branch['histories'][0]['started_at'] == null){
								$class_branch = 'warning_red';
						}
				}
				if (count($branch['histories']) > 0) {
						$type = 1;
						$id = $branch['histories'][0]['id'];
				}else{
						$type = 0;
						$id = $branch['branch']['id'];
				}


			 //var_dump($branch['authorized']);
			 $branch_full_name = $branch['branch']['site_code'] . " " . $branch['branch']['name'];
?>
<tr class="tr_branch <?= $class_branch ?>" onclick="<?= ( $branch['authorized'] ) ? 'checkModal('.$type.',\''.$branch_full_name.'\','.$id.', '.$_SESSION["id"].')' : 'alert(\'Outside the site range\')' ?> ">

		<td scope="row">

				<?= $branch['branch']['site_code'] ?>

		</td>
		<td>

				<?= $branch['branch']['name'] ?>

		</td>
		<td>

				<?= $branch['client']['name'] ?>

		</td>
		<td>

				<?= $branch['branch']['address'] ?>

		</td>
		<td>

				<?= $branch['branch']['city'] ?>

		</td>
		<td>

				<?= $branch['branch']['state'] ?>

		</td>
		<td>

				<?= $branch['branch']['zipcode'] ?>

		</td>
		<td>
			<?php if (!empty($branch['branch']['max_time'])) { ?>
				<?= explode('.000', explode('T', $branch['branch']['max_time'])[1])[0] ?>
			<?php } ?>

		</td>

		<td>
				<?php if (!empty($branch['histories'][0]['started_at'])) { ?>
				<?= ($branch['histories'][0]['started_at'] != null) ? date( 'Y-m-d H:i:s',strtotime($branch['histories'][0]['started_at'])) : '' ?>
			<?php } ?>

		</td>
		<td>
			<?php if (!empty($branch['histories'][0]['finished_at'])) { ?>
				<?= ($branch['histories'][0]['finished_at']!= null) ? date( 'Y-m-d H:i:s',strtotime($branch['histories'][0]['finished_at'])) : ''; ?>
<?php } ?>
		</td>


</tr>
<?php } }?>
