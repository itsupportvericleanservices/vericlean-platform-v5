<?php 
include('../includes/header.php');
$frequency = vcGetFrequency($_SESSION['access-token'], $_GET['id']);
?>

<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">frequency: <?php echo $frequency['name'] ?> </h3>
            </div>
        </div>
    </div>



    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                        <!--Begin::Section-->
                        <div class="m-portlet">
							<div class="m-portlet__body m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
									<div class="col-md-12 col-lg-12 col-xl-12">
										<!--begin:: Widgets/Stats2-1 -->
										<div class="m-widget1">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col text-center">
                                                        <span class="m-widget1__title">Name:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $frequency['name'] ?></h3>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col text-center">
                                                        <span class="m-widget1__title">Days:</span>
														<h3 class="m-widget1__title m--font-brand"><?= $frequency['days'] ?></h3>
													</div>
												</div>
											</div>
										</div>
										<!--end:: Widgets/Stats2-1 -->
                                    </div>
								</div>
							</div>
						</div>
                    </div>
                </div>
        <!--END XL12-->
     </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->
