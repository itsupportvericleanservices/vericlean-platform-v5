<?php 
include('../includes/header.php');
$photo = vcGetPhoto($_SESSION['access-token'], $_GET['id']);
?>

<style>
    .example-image{
        width:200px !important;
    }
</style>


<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

<?php include('../includes/sidebar_menu.php');?>

<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Photos of: <?php echo $photo[0]['user']['first_name']; ?> 
            in Service <?php echo $photo[0]['service']['id']; ?></h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
            <div class="col-xl-12">

                        <!--Begin::Section-->
                        <div class="m-portlet">
							<div class="m-portlet__body m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-md-12 col-lg-12 col-xl-12">

                                        <div class="card">
                                            <a class="example-image-link" href="<?= $photo[1]['file_url'] ?>" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="<?= $photo[1]['file_url'] ?>" alt=""/></a>
                                        </div>
                              
                                    </div>        
								</div>
							</div>
						</div>
                    </div>
                </div>
        <!--END XL12-->
     </div>
    <!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->

<script src="../assets/js/plugins/lightbox-plus-jquery.min.js"></script>