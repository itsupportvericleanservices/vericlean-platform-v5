<?php
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$token = $json['token'];
$message = $json['message'];
// prep the bundle
$msg = array(
	'body' 	=> $message,
	'title'		=> 'Vericlean',
);
$fields = array(
	'to' => $token,
	'notification' => $msg, 
  'priority' => 'high'
);
 
$headers = array('Authorization: key=AIzaSyDgITgco0XJcOUPqURAUASkS56k6ffmF8Q','Content-Type: application/json');
$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch);
curl_close( $ch );

$response = $result;
echo json_encode($response);
?>