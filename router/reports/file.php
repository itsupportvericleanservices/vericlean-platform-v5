<?php
// Include Functions
include('../../includes/functions.php');

$data =getReportNoCheckins($_SESSION['access-token'],$_GET['by'],$_GET['id'],$_GET['start'],$_GET['finished']);
$sites = $data['sites'];
$clients = $data['clients'];
$checks = $data['checkins'];
//YmdHis
$txtDate = ( $_GET['start'] === $_GET['finished'] )  ? date('Ymd',strtotime($_GET['start'])) : date('Ymd',strtotime($_GET['start']))."-".date('Ymd',strtotime($_GET['finished'])) ;

header("Content-Type: application/vnd.ms-excel");
header("Expires: 0&#8243;");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0&#8243;");
header("content-disposition: attachment;filename=report-no-checkins-".$txtDate.".xls");
?>

<div class="m-section" id="app" >
    <div class="table-responsive">
      <?php foreach ($checks as $key => $value) { ?>
      <table  class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
        <thead>
          <tr>
            <th>ID</th>
            <th width="70px">Site Code</th>
            <th>Name</th>
            <th>Type</th>
            <th>Client</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            <th>Frequency</th>
            <th>Cleaner</th>
            <th>Cleaner Phone</th>
            <th>Area Manager</th>
            <th>Area Manager Phone</th>
            <th>Supervisor</th>
            <th>Supervisor Phone</th>
            <th>Active</th>
            <th>Muted</th>
            <th>Min duration on site</th>
            <th>Max duration on site</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($value as $key => $branch) {?>
          <tr>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p >
                <?= $branch['site_code'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['name'] ?>
              </p>
              
            </td>
            <td>
              <?php if($branch['client_indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
            </td>
            <td>
              <?= $branch['client_name'] ?>
            </td>
            <td>
              <p >
                <?php echo $branch['address'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['city'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['state'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['zipcode'] ?>
              </p>
              
            </td>
            <td>
              <p>
                <?php if(empty($branch['frequency'])){ ?>
                not assigned
                <?php }else{ ?>
                <?php
							$frequencies = explode(",", $branch['frequency']);
								foreach($frequencies as $freq){
									if($freq == 1){
										echo "Monday ";
									}
									if($freq == 2){
										echo "Tuesday ";
									}
									if($freq == 3){
										echo "Wednesday ";
									}
									if($freq == 4){
										echo "Thursday ";
									}
									if($freq == 5){
										echo "Friday ";
									}
									if($freq == 6){
										echo "Saturday ";
									}
									if($freq == 7){
										echo "Sunday ";
									}
								}
							?>
                <?php } ?>
              </p>
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner_name'] ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['cleaner_phone']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['areamanager_phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['areamanager_phone']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['supervisor_phone'])){ ?>
                not phone
                <?php }else{ ?>
                <?= $branch['supervisor_phone']  ?>
                <?php } ?>
              </p>
             
            </td>
            <td>
              <a href="show.php?id=<?= $branch['id'] ?>">
                <?php if($branch['active'] == 1){ ?>
                <span class="m-badge m-badge--success m-badge--wide">Active</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Inactive</span>
                <?php } ?>
              </a>
            </td>
            <td>
              <a href="show.php?id=<?= $branch['id'] ?>">
                <?php if($branch['muted'] == 1){ ?>
                <span class="m-badge m-badge--danger m-badge--wide">Without alerts</span>
                <?php }else{ ?>
                <span class="m-badge m-badge--success m-badge--wide">With alerts</span>
                <?php } ?>
              </a>
            </td>
            <td>
              <p >
                <?= $branch['duration_min'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?= $branch['duration_max'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php $rest = substr($branch['max_time'], 11, 5); echo $rest ?>
              </p>
              
            </td>
            
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } ?>
    </div>
  </div>

</div>
   

<!-- <script src="../assets/js/jquery.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script> -->

<!-- <script>

  /* Defaults */
  var exportTable = document.getElementsByTagName("table")
  var export_tables = new TableExport(exportTable, {
      formats: ['xlsx'],
      bootstrap: true,
      exportButtons: false,
      // ignoreRows: null,             
      // ignoreCols: 10,

  }); 
  var tables_data = export_tables.getExportData()
  var export_data = []
  var xlsx_info = {}
  for (table_id in tables_data){
      xlsx_info = tables_data[table_id]["xlsx"]
      export_data.push(tables_data[table_id]["xlsx"].data)
  }
  var fileExtension = xlsx_info.fileExtension
  var mimeType = xlsx_info.mimeType
  $(document).ready(function(){
      export_tables.exportmultisheet(export_data, mimeType, "Reporte", [],
                                    fileExtension, {}, [])
  })

</script> -->