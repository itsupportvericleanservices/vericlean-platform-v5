<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$response = vcGetSitesForClient($_SESSION['access-token'], $json['client_id']);
echo json_encode($response);


?>
