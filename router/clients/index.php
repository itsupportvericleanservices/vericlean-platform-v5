<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);


if($json['type'] == 'client' || $json['type'] == 'customer'){
    $clients = vcGetClientFilter($_SESSION['access-token'], $json['id']);
}

if($json['type'] == 'customer_for_client'){
    $clients = vcGetCustomersForClientFilter($_SESSION['access-token'], $json['client_id'], $json['active']);
}

if(empty($json['type'])){
    $clients = vcGetActiveClients($_SESSION['access-token']);
}
?>


                        <div class="m-section" v-if="client == '' ">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Main Account</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Addres2</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>ZipCode</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Active</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($clients as $key => $client) {
                                        ?>
                                        <tr>
                                            <td scope="row">
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['id'] ?>
                                                </a>
                                            </td>
                                            <td scope="row">
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                     <?php if($client['indirect'] == 1){echo "Customer";} else {echo "Client";}; ?>
                                                </a>
                                            </td>
                                            <td scope="row">
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?php if($client['indirect'] == 1){
                                                       $main_client = vcGetClient($_SESSION['access-token'], $client['indirect_id']);
                                                       echo $main_client['name'];
                                                    } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['name'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['address'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['address2'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['state'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['city'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['zipcode'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['phone'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $client['id'] ?>">
                                                    <?= $client['email'] ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="show.php?id=<?= $branch['id'] ?>">
                                                    <?php if($client['active'] == 1){ ?>
                                                        <span class="m-badge m-badge--success m-badge--wide">Active</span>
                                                    <?php }else{ ?>
                                                        <span class="m-badge m-badge--danger m-badge--wide">Inactive</span>
                                                    <?php } ?>
                                                </a>
                                            </td>
                                            <td>
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                                        data-toggle="dropdown" aria-expanded="true">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="show.php?id=<?= $client['id'] ?>"><i class="la la-crosshairs"></i>
                                                            View</a>
                                                        <a class="dropdown-item" href="update.php?id=<?= $client['id'] ?>"><i class="la la-edit"></i>
                                                            Update</a>
                                                        <a onclick="deleteClient(event, <?= $client['id'] ?>)" class="dropdown-item"><i class="la la-trash"></i>
                                                            Delete</a>
                                                    </div>
                                                </span>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="m-section" v-if="client != ''">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <!-- <th>Main Account</th> -->
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Addres2</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>ZipCode</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Active</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                           <tr v-for="cliente in clients">
                                               <td>{{cliente.id}}</td>
                                               
                                               <td>{{cliente.type}}</td>
                                               <!-- <td></td> -->
                                               <td>{{cliente.name}}</td>
                                               
                                               <td>{{cliente.address}}</td>
                                               <td>{{cliente.address2}}</td>
                                               <td>{{cliente.state}}</td>
                                               <td>{{cliente.city}}</td>
                                               <td>{{cliente.zipcode}}</td>
                                               <td>{{cliente.phone}}</td>
                                               <td>{{cliente.email}}</td>
                                               <td><small style="background: lightgreen;padding: 3px;border-radius: 3px;">{{cliente.active == 1 ? "Active" : "inactive" }}</small></td>
                                               <td>
                                                <span class="dropdown">
                                                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                                        data-toggle="dropdown" aria-expanded="true">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a @click="window.location = `show.php?id=${cliente.id}`" class="dropdown-item" ><i class="la la-crosshairs"></i>
                                                            View</a>
                                                        <a class="dropdown-item" @click="window.location = `update.php?id=${cliente.id}`"><i class="la la-edit"></i>
                                                            Update</a>
                                                        <a @click="_deleteClient(event, cliente.id)" class="dropdown-item"><i class="la la-trash"></i>
                                                            Delete</a>
                                                    </div>
                                                </span>
                                            </td>
                                           </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


<script>
    const clientes = <?php echo json_encode($clients) ?>;
    console.log(clientes);
    new Vue({
        el:"#app2",
        data:{
            search: false,
            clients: '',
            client:""
        },
        computed: {
        stg(){
            let string = this.client.toLowerCase()
            return string.charAt(0).toUpperCase() + string.slice(1);
            },
        stgUp(){
          
            return this.client.toUpperCase()
            }
        
        },
        
        methods:{
            filter_client(){
                this.clients = clientes.filter(el=>{
                 return   el.name.includes(this.stg) || el.name.includes(this.stgUp)
                })
            },
            _deleteClient(event, id) {
                event.preventDefault();
                Swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "../../../router/clients/delete.php?id=" + id,
                        dataType: "json"
                    }).done(function (data) {
                        if (data == true) {
                        swal({
                            title: "Success!",
                            text: "Customer Deleted",
                            icon: "success",
                            button: "Ok",
                            closeOnEsc: true
                        }).then(result => {
                            location.reload()
                        });
                        } else {
                        Swal({
                            type: 'error',
                            title: 'You can´t delete the client',
                            text: 'The customer is in use',
                        })
                        }
                    }).error(function (err) {
                        swal({
                        title: 'Error!',
                        text: 'Something happened!',
                        type: 'error',
                        confirmButtonText: 'Cool'
                        })
                    });
                    }

                })
            }


        }
    })
</script>    