
<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);
// $sites = vcGetBranches($_SESSION['access-token']);
// $clients = vcGetClients($_SESSION['access-token']);
$data = getChecksClnr($_SESSION['access-token'],$_GET['id'],$_GET['branch'],$_GET['by'],$_GET['start'],$_GET['finished']);
$clnr = $data['cleaners'];
$checks = $data['checkins']; 
$supervisors = $data['supervisors']; 
// $completes = $data['completes'];
// $incompletes = $data['incompletes'];
$all = $data['all'];
$vals = [];
$vals_h = [];
$vals_m = [];
foreach ($checks as $check) {
  $fecha1 = new DateTime($check['route_histories_started']);
  $fecha2 = new DateTime($check['route_histories_finished']);

  $intervalo = $fecha1->diff($fecha2);

  $all_time = $intervalo->format('%H:%i');

  $h = substr($all_time,0,2);

  array_push($vals_h,(int)$h);

  $m = substr($all_time,3,2);

  array_push($vals_m,(int)$m);

  array_push($vals,new DateTime($all_time));
}
$total_h = 0;
$total_m = 0;

foreach ($vals_h as $value) {
 $total_h += $value;
}

foreach ($vals_m as $value) {
  $total_m += $value;
 }

$total_m_h = $total_m/60; 

// $hours_sum = (int)substr($total_m_h,0,2);
// $min_rest = $total_m - $hours_sum * 60 

$total_horas = $total_h + $total_m_h;
if (count($checks) > 0) {
  $count = $total_horas/count($checks);
}else {
  $count = 0;
}


header("Content-Type: application/vnd.ms-excel");
header("Expires: 0&#8243;");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0&#8243;");
header("content-disposition: attachment;filename=report-checkins-duration".date('YmdHis').".xls");
?>

<?php if ($checks != null) {?>
<div class="m-section" id="app" >
    <div class="table-responsive" style="<?php  if(count($branchesWithPage[0]) < 3){ echo "height: 260px;"; } ?>">
      
    

         <!-- <h5><small>Date:</small> <span style="background:green;color:white;padding:5px;border-radius:5px;"><?= $key?></span> </h5> -->
        <table  class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
        <thead>
          <tr >
            <th>ID</th>
            <th>Site Code</th>
            <th>Name</th>
            <th width="35px">Duration on site</th>
            <th width="35px">Started At</th>
            <th>Finished At</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Client</th>
            <th>Cleaner</th>
            <th>Area Manager</th>
            <th>Supervisor</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($checks as $key => $branch) { ?>
          <?php  
            $fecha1 = new DateTime($branch['route_histories_started']);//fecha inicial
            $fecha2 = new DateTime($branch['route_histories_finished']);//fecha de cierre

            $intervalo = $fecha1->diff($fecha2);

            
          ?>
          <tr <?php if($branch['route_histories_status'] == 1){echo 'style="background: #eee;"';}?>>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p >
                <?= $branch['site_code'] ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php echo $branch['name'] ?>
              </p>
              
            </td>
            <td>
            <p>
                
              <span style="width:75px;" class="m-badge m-badge--warning m-badge--wide"><?= $intervalo->format(' %H : %i'); ?></span>
                
              </p>
              
            </td>
            <td>
              <p >
                
                <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_started'] ."UTC")); ?></span>
              </p>
              
            </td>
            <td>
              <p >
                <?php if(empty($branch['route_histories_finished'])){ ?>
                  No Info
                <?php }else{ ?>
                <span class="m-badge m-badge--success m-badge--wide" style="margin-bottom: 5px;width: 90px;"><?php echo date('m-d-Y', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <span class="m-badge m-badge--info m-badge--wide" style="width: 90px;"><?php echo date('H:i', strtotime($branch['route_histories_finished'] ."UTC")); ?></span>
                <?php } ?>
              </p>
            </td>
            <td>
              <?= $branch['address'] ?>
            </td>
            <td>
              <?= $branch['city'] ?>
            </td>
            <td>
              <?= $branch['state'] ?>
            </td>
            <td>
              <?= $branch['client_name'] ?>
            </td>
            <td>
              <p >
                <?php if(empty($branch['cleaner_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner_name'] ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>
              
            </td>

            <td>
              <p >
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>
              
            </td>
            <td>
              <p >
                <?php $rest = substr($branch['max_time'], 11, 5); echo $rest ?>
              </p>
              
            </td>
            
          </tr>
          <?php } ?>
        </tbody>
      </table>
     
    </div>
  </div>

</div>
<?php } ?>