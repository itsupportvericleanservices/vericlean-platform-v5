<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

$response = htdevsCustom($_SESSION['access-token'], '/branches/'.$_GET['site_code'].'/getbysitecode');
echo json_encode($response);
?>
