<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

// $filePath = $_FILES['excel']['tmp_name'];
// $fileInfo = ['file_path' => $filePath, 'file_name' => 'excel'];

// $response = uploadExcel($_SESSION['access-token'], $_FILES, $fileInfo);
$response = processExcel($_SESSION['access-token'], $json);
echo json_encode($response);
?>
