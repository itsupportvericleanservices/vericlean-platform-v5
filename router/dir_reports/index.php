<?php
// Include Functions
include('../../includes/functions.php');

// Get JSON
$json = file_get_contents('php://input');
$json = json_decode($json, true);

// $sites = vcGetBranches($_SESSION['access-token']);
// $clients = vcGetClients($_SESSION['access-token']);
$data = postDirReports($_SESSION['access-token'],$json);

$clients = $data['clients'];
$customers = $data['customers'];

$cleaners=$data['cleaners'];
$supervisors = $data['supervisors'];

$states = $data['states'];
$cities = $data['cities'];

$status = [
  'Complete',
  'Complete UT',
  'Incomplete',
  'No Show'
];

$page = $data['page']; 
$pages = $data['pages']; 
$per_page = $data['per_page']; 

$checks = $data['checkins']; 
$completes = $data['completes'];
$completes_ut = $data['completes_ut'];
$incompletes = $data['incompletes'];
$no_show = $data['no_show'];
$all = $data['all'];
if ($all > 0) {
$porsentaje =  $completes/$all*100;
$comp = round($porsentaje);

$porsentaje =  $completes_ut/$all*100;
$comp_ut = round($porsentaje);

$porsentaje_in =  $incompletes/$all*100;
$incomp = round($porsentaje_in);

$porsentaje_in =  $no_show/$all*100;
$no_sh = round($porsentaje_in);
}else{
  $comp = 0;
  $incomp = 0;
}
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper cards-info">
  <div class="m-portlet ">
    <div class="m-portlet__body  m-portlet__body--no-padding">
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-sm-6" style="height: 160px;">
          <div class="m-widget24">

            <div class="m-widget24__item" style="text-align:center">
              <div class="row" style="max-height: 10px;">
                <h5 class="title_card_first_left">
                  Checkins
                </h5><br>
                <h5 class="title_card_first_right">
                  Checkins
                </h5><br>
              </div>
              <div class="m--space-30"></div>
              <div class="row">
                <span class="m-widget24__desc " style="position:absolute; left:0%; top:35px">
                  Total
                </span>
                <span class="m-widget24__desc" style="position:absolute; right:2%; top: 35px;">
                  Completes
                </span>
              </div>
              <div class="row">
                <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                  <?= $data['all']?>
                </span>
                <span class="m-widget24__stats m--font-success number_card_right"
                  style="position:absolute; right:0%; top:60px; font-size: 40px;">
                  <?= $data['completes']?>
                </span>
              </div>
              <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                <div class="progress-bar m--bg-success" role="progressbar" style="width: <?php echo $comp.'%' ?>;"
                  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                percentage
              </span>
              <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                <?php echo $comp ?>%
              </span>
            </div>
          </div>
        </div>

        <div class="col-sm-6" style="height: 160px;">
          <div class="m-widget24">

            <div class="m-widget24__item" style="text-align:center">
              <div class="row" style="max-height: 10px;">
                <h5 class="title_card_second_left">
                  Checkins
                </h5><br>
                <h5 class="title_card_second_right">
                  Checkins
                </h5><br>
              </div>
              <div class="m--space-30"></div>
              <div class="row">
                <span class="m-widget24__desc description_card_second description_card_second_left"
                  style="position:absolute; left:0%; top:35px">
                  Total
                </span>
                <span class="m-widget24__desc description_card_second" style="position:absolute; right:2%; top: 35px;">
                  Completes UT
                </span>
              </div>
              <div class="row">
                <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                  <?= $all ?>
                </span>

                <span class="m-widget24__stats m--font-info number_card_second_right"
                  style="position:absolute; right:-3%; top:60px; font-size: 40px;">
                  <?= $completes_ut?>
                </span>
              </div>
              <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                <div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $comp_ut.'%' ?>;"
                  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                percentage
              </span>
              <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                <?php echo $comp_ut ?>%
              </span>
            </div>
          </div>
        </div>
      </div>
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-sm-6" style="height: 160px;">
          <div class="m-widget24">

            <div class="m-widget24__item" style="text-align:center">
              <div class="row" style="max-height: 10px;">
                <h5 class="title_card_third_left">
                  Checkins
                </h5><br>
                <h5 class="title_card_third_right">
                  Checkins
                </h5><br>
              </div>
              <div class="m--space-30"></div>
              <div class="row">
                <span class="m-widget24__desc " style="position:absolute; left:0%; top:35px">
                  Total
                </span>
                <span class="m-widget24__desc" style="position:absolute; right:2%; top: 35px;">
                  Incompletes
                </span>
              </div>
              <div class="row">
                <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                  <?= $data['all']?>
                </span>
                <span class="m-widget24__stats m--font-warning number_card_right"
                  style="position:absolute; right:0%; top:60px; font-size: 40px;">
                  <?= $data['incompletes']?>
                </span>
              </div>
              <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                <div class="progress-bar m--bg-warning" role="progressbar" style="width: <?php echo $incomp.'%' ?>;"
                  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                percentage
              </span>
              <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                <?php echo $incomp ?>%
              </span>
            </div>
          </div>
        </div>

        <div class="col-sm-6" style="height: 160px;">
          <div class="m-widget24">

            <div class="m-widget24__item" style="text-align:center">
              <div class="row" style="max-height: 10px;">
                <h5 class="title_card_quarter_left">
                  Checkins
                </h5><br>
                <h5 class="title_card_quarter_right">
                  Checkins
                </h5><br>
              </div>
              <div class="m--space-30"></div>
              <div class="row">
                <span class="m-widget24__desc description_card_second description_card_second_left"
                  style="position:absolute; left:0%; top:35px">
                  Total
                </span>
                <span class="m-widget24__desc description_card_second" style="position:absolute; right:2%; top: 35px;">
                  No Show
                </span>
              </div>
              <div class="row">
                <span class="m-widget24__stats" style="position:absolute; left:10%; top:60px; font-size: 40px;">
                  <?= $all ?>
                </span>

                <span class="m-widget24__stats m--font-danger number_card_second_right"
                  style="position:absolute; right:-3%; top:60px; font-size: 40px;">
                  <?= $no_show?>
                </span>
              </div>
              <div class="progress m-progress--sm progress-margin" style="position: relative; bottom: -85px;">
                <div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo $no_sh.'%' ?>;"
                  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <span class="m-widget24__change" style="position: relative; bottom: -85px;">
                percentage
              </span>
              <span class="m-widget24__number" style="position: relative; bottom: -85px;">
                <?php echo $no_sh ?>%
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- <pre><?php  print_r($checks[0])?></pre> -->

<!-- -------------- -->
<div class="m-portlet__head" style="height: 300px;">
  <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">

      <div class="m-portlet__head-text">
        <div class="form-inline">

          <div class="form-group m-3">

            <label>Range &nbsp;</label>
            <div >
              <input type="text" name="range" id="from_date_filter_form" class="form-control" value="">
            </div>

          </div>

          <div class="form-group m-3">

            <label>Select Client &nbsp;</label>
            <div class="">

              <div>
                <select name="client_id" class="client_id_form form-control" onchange="showButton()">
                  <option value="" <?php if($json['id']==''){echo 'selected' ;}?>>View All</option>
                  <?php 
                  usort($clients, function($a, $b) {
                    return strtolower($a[1]) > strtolower($b[1]);
                    
                    });
                  foreach ($clients as $key => $client) { ?>
                  <option value="<?= $client[0] ?>" <?php if($json['client_id']==$client[0]){echo 'selected' ;}?>>
                    <?= $client[1] ?>
                  </option>

                  <?php  } ?>
                </select>
              </div>
            </div>

          </div>

          <div class="form-group m-3">

            <label>Select Customer &nbsp;</label>
            <div class="">

              <div>
                <select name="customer_id" class="customer_id_form form-control" onchange="showButton()">
                  <option value="" <?php if($json['id']==''){echo 'selected' ;}?>>View All</option>
                  <?php 
                  usort($customers, function($a, $b) {
                    return strtolower($a[1]) > strtolower($b[1]);
                    
                    });
                  foreach ($customers as $key => $customer) { ?>
                  <option value="<?= $customer[0] ?>" <?php if($json['customer_id']==$customer[0]){echo 'selected' ;}?>>
                    <?= $customer[1] ?>
                  </option>

                  <?php  } ?>
                </select>
              </div>
            </div>

          </div>

          <div class="form-group m-3">

            <label>Site &nbsp;</label>
            <div class="">

              <div>
                <input type="text" value="<?=($json['site']=="")?'':$json['site'] ?>" name="site"
                  class="site_form form-control" onchange="showButton()" placeholder="Search a name site">
              </div>
            </div>

          </div>

          <div class="form-group m-3">

            <label>Address &nbsp;</label>
            <div class="">

              <div>
                <input type="text" value="<?=($json['address']=="")?'':$json['address'] ?>" name="address"
                  class="address_form form-control" onchange="showButton()" placeholder="Search a name address">
              </div>
            </div>

          </div>
        
          <div class="form-group m-3">
  
            <label>Select Cleaner &nbsp;</label>
            <div class="">
  
              <div>
                <select name="cleaner_id" class="cleaner_id_form form-control" onchange="showButton()">
                  <option value="" <?php if($json['id']==''){echo 'selected' ;}?>>View All</option>
                  <?php 
                    usort($cleaners, function($a, $b) {
                      return strtolower($a[1]) > strtolower($b[1]);
                      
                      });
                    foreach ($cleaners as $key => $cleaner) { ?>
                  <option value="<?= $cleaner[0] ?>" <?php if($json['cleaner_id']==$cleaner[0]){echo 'selected' ;}?>>
                    <?= $cleaner[1]." ".$cleaner[2] ?>
                  </option>
  
                  <?php  } ?>
                </select>
              </div>
            </div>
  
          </div>
  
          <div class="form-group m-3">
            <label>Select supervisor &nbsp;</label>
            <div class="">
    
              <div>
                <select name="supervisor_id" class="sup_id_form form-control" onchange="showButton()">
                  <option value="" <?php if($json['id']==''){echo 'selected' ;}?>>View All</option>
                  <?php 
                  usort($supervisors, function($a, $b) {
                    return strtolower($a[1]) > strtolower($b[1]);
                    
                    });
                  foreach ($supervisors as $key => $sup) { ?>
                  <option value="<?= $sup[0] ?>" <?php if($json['supervisor_id']==$sup[0]){echo 'selected' ;}?>>
                    <?= $sup[1]." ".$sup[2] ?>
                  </option>
    
                  <?php  } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group m-3">
            
            <label>Select State &nbsp;</label>
            <div class="">
    
              <div>
                <select name="state_id" class="state_id_form form-control" onchange="showButton()">
                  <option value="" <?php if($json['id']==''){echo 'selected' ;}?>>View All</option>
                  <?php 
                  usort($states, function($a, $b) {
            return strtolower($a[1]) > strtolower($b[1]);
                    
                    });
                  foreach ($states as $key => $state) { ?>
                  <option value="<?= $state ?>" <?php if(strval($json['state'])==$state){echo 'selected' ;}?>>
                    <?= $state ?>
                  </option>
    
                  <?php  } ?>
                </select>
              </div>
            </div>
  
          </div>
          <div class="form-group m-3">
            
            <label>Select City &nbsp;</label>
            <div class="">
    
              <div>
                <select name="city_id" class="city_id_form form-control" onchange="showButton()">
                  <option value="" <?php if($json['id']==''){echo 'selected' ;}?>>View All</option>
                  <?php 
                  usort($cities, function($a, $b) {
                    return strtolower($a[1]) > strtolower($b[1]);
                    
                    });
                    foreach ($cities as $key => $city) { ?>
                  <option value="<?= $city[1] ?>" <?php if(strval($json['city'])==$city[1]){echo 'selected' ;}?>>
                    <?= $city[1] ?>
                  </option>
    
                  <?php  } ?>
                </select>
              </div>
            </div>
  
          </div>
          <div class="form-group m-3">
            
            <label>Select Status &nbsp;</label>
            <div class="">
    
              <div>
                <select name="status_id" class="status_id_form form-control" onchange="showButton()">
                  <option value="" <?php if($json['id']==''){echo 'selected' ;}?>>View All</option>
                  <?php 
                  foreach ($status as $key => $statu) { ?>
                  <option value="<?= $key+1 ?>" <?php if($json['status']==$key+1){echo 'selected' ;}?>>
                    <?= $statu ?>
                  </option>
                  <?php  } ?>
                </select>
              </div>
    
            </div>
  
          </div>
          <div class="form-group m-3">
            <div class="filter" style="padding: 5px;">
              <button onclick="postDirReports('form',page)" class="btn btn-info">Filter</button>
            </div>
            <div class="clear" style="padding: 5px;">
              <a href="index.php" class="btn btn-danger">Clear</a>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
  <div class="m-portlet__head-tools">
    <ul class="m-portlet__nav">
      <li class="m-portlet__nav-item">
        <a class="btn btn-secondary" href='../../router/dir_reports/file.php?client_id=<?=$json['client_id']?>&customer_id=<?=$json['customer_id']?>&site=<?=$json['site']?>&address=<?=$json['address']?>&cleaner_id=<?=$json['cleaner_id']?>&supervisor_id=<?=$json['supervisor_id']?>&state=<?=$json['state']?>&city=<?=$json['city']?>&status=<?=$json['status']?>&start=<?= $json['start'] ?>&finished=<?= $json['finished'] ?>&page=<?= $json['page'] ?>&per_page=<?= $json['per_page'] ?>' >Export
        </a>

      </li>
    </ul>
  </div>
</div>

<div class="m-portlet__body">
  <!--begin::Section-->
  <!--end::Section-->
  <!-- <pre><?php print_r($sites)?></pre> -->
  <?php if ($checks != null) {?>
    
    <div>Total Pages: <?= $pages?></div>
    <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-end">
        <li class="page-item <?= ($page-1) < 1 ? 'disabled' : '' ?>">
          <a class="page-link" onclick="postDirReports('form',1)" href="#" tabindex="-1">First</a>
        </li>

        <?php for ($i=2; $i < 8; $i++) { ?>  
          <?php if(($page-$i)>1) { ?>
            <li class="page-item"><a class="page-link" onclick="postDirReports('form',<?= $page-$i ?>)" href="#"><?= $page-$i ?></a></li>
          <?php } ?>
        <?php } ?>
        
        <?php if(($page-1)>=1) { ?>
          <li class="page-item"><a class="page-link" onclick="postDirReports('form',<?= $page-1 ?>)" href="#"><?= $page-1 ?></a></li>
        <?php } ?>


        <li class="page-item active"><a class="page-link" onclick="postDirReports('form',<?= $page ?>)" href="#"><?= $page ?></a></li>
        
        
        <?php if(($page+1)<=$pages) { ?>
          <li class="page-item"><a class="page-link" onclick="postDirReports('form',<?= $page+1 ?>)" href="#"><?= $page+1 ?></a></li>
        <?php } ?>
        <?php for ($i=2; $i < 8; $i++) { ?> 
          <?php if(($page+$i)<=$pages) { ?>
            <li class="page-item"><a class="page-link" onclick="postDirReports('form',<?= $page+$i ?>)" href="#"><?= $page+$i ?></a></li>
          <?php } ?>
        <?php } ?>

        <li class="page-item <?= ($page-1) > $pages ? 'disabled' : '' ?>">
          <a class="page-link" onclick="postDirReports('form',<?= $pages ?>)" href="#">Last</a>
        </li>
      </ul>
    </nav>
    
  <div class="m-section" id="app">
    <div class="table-responsive">



      <!-- <h5><small>Date:</small> <span style="background:green;color:white;padding:5px;border-radius:5px;"><?= $key?></span> </h5> -->
      <table class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
        <thead>
          <tr>
            <th>ID</th>
            <th>Site Code</th>
            <th>Client</th>
            <th>Customer</th>
            <th>Site</th>
            <th>Date</th>
            <th width="35px">Status</th>
            <th width="35px">Started At</th>
            <th>Finished At</th>
            <th>Time Elapsed</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Cleaner</th>
            <th>Area Manager</th>
            <th>Supervisor</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($checks as $key => $branch) { ?>
          <tr <?php if($branch['route_histories_status']==1){echo 'style="background: #eee;"' ;}?>>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p>
                <?= $branch['site_code'] ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $branch['client'] ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $branch['customer'] ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $branch['site'] ?>
              </p>

            </td>
            <td>
              <p>
                <?php if(empty($branch['date'])){ ?>
                not assigned
                <?php }else{ ?>
                <?php echo $branch['date'] ?>
                <?php } ?>
              </p>
            </td>
            <td>
              <p>
                <?php  
                  switch ($branch['status']) {
                    case '4':
                      $bagde = "danger";
                      $status_msg = "No Show";
                      break;
                    case '3':
                      $bagde = "warning";
                      $status_msg = "Incomplete";
                      break;
                    case '2':
                      $bagde = "info";
                      $status_msg = "Complete";
                      break;
                    case '1':
                      $bagde = "success";
                      $status_msg = "Complete";
                      break;
                      
                  }
                ?>
                <span class="m-badge m-badge--<?=$bagde;?> m-badge--wide">
                  <?= $status_msg; ?>
                </span>
              </p>

            </td>
            <td>
              <?php if(empty($branch['start_time'])){ ?>
              No Info
              <?php }else{ ?>
              <span class="m-badge m-badge--secondary m-badge--wide" style="margin-bottom: 5px;width: 90px;">
                <?php echo date('m-d-Y', strtotime($branch['start_time'] ."UTC")); ?>
              </span>
              <span class="m-badge m-badge--secondary m-badge--wide" style="width: 90px;">
                <?php echo date('H:i', strtotime($branch['start_time'] ."UTC")); ?>
              </span>
              <?php } ?>

            </td>
            <td>
              <p>
                <?php if(empty($branch['end_time'])){ ?>
                No Info
                <?php }else{ ?>
                <span class="m-badge m-badge--secondary m-badge--wide" style="margin-bottom: 5px;width: 90px;">
                  <?php echo date('m-d-Y', strtotime($branch['end_time'] ."UTC")); ?>
                </span>
                <span class="m-badge m-badge--secondary m-badge--wide" style="width: 90px;">
                  <?php echo date('H:i', strtotime($branch['end_time'] ."UTC")); ?>
                </span>
                <?php } ?>
              </p>
            </td>
            <td>
              <p>
                <?php echo $branch['time_elapsed'] ?>
              </p>

            </td>
            <td>
              <?= $branch['address'] ?>
            </td>
            <td>
              <?= $branch['city'] ?>
            </td>
            <td>
              <?= $branch['state'] ?>
            </td>
            <td>
              <p>
                <?php if(empty($branch['cleaner_name'])){ ?>
                <span class="m-badge m-badge--secondary m-badge--wide">
                  not assigned
                </span>
                <?php }else{ ?>
                <span class="m-badge m-badge--primary m-badge--wide">
                <?= $branch['cleaner_name'] ?>
                </span>
                <?php } ?>
              </p>

            </td>

            <td>
              <p>
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>

            </td>

            <td>
              <p>
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $date =  date('H:i', strtotime($branch['max_time'])  - 21600);    //$rest = substr($branch['max_time'], 11, 5); echo $rest
                ?>
              </p>

            </td>

          </tr>
          <?php } ?>
        </tbody>
      </table>

    </div>
  </div>

</div>
<?php } ?>
<!--begin::BRANCHES -->

<script src="../assets/js/vue.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script>
<script src="../assets/js/reports/index.js"></script>
<script>
  $('input[name="range"]').daterangepicker({
    locale: {
      format: 'MM-DD-YYYY'
    }
  });
</script>

<script>

  $(document).ready(function () {

    /* Defaults */
    var exportTable = $("table")
    var export_tables = new TableExport(exportTable, {
      formats: ['xlsx'],
      bootstrap: true,
      exportButtons: false,
      // ignoreRows: null,             
      // ignoreCols: 10,
      
    });
    var tables_data = export_tables.getExportData()
    
    var export_data = []
    var xlsx_info = {}
    for (table_id in tables_data) {
      xlsx_info = tables_data[table_id]["xlsx"]
      export_data.push(tables_data[table_id]["xlsx"].data)
    }
    var fileExtension = xlsx_info.fileExtension
    var mimeType = xlsx_info.mimeType
    $("#export_btn").click(function () {
      
      export_tables.exportmultisheet(export_data, mimeType, "Reporte ", [], fileExtension, {}, [])
    })
      <?php 

    $customers = json_encode($customers);
    $clients = json_encode($clients);

  ?>
  
  var clients = <?= $clients ?>;
  var customers = <?= $customers ?>;
  console.log(customers);
    $(".client_id_form").change(function () {
      let selected_client = $(this).val();
      if (selected_client == "") {
        $(".customer_id_form").empty();
        $(".customer_id_form").append(
          "<option value=''>View All</option>"
        );
        $.each(customers, function (key, item) {
          $(".customer_id_form").append(
            "<option value='" + item[0] + "'>" + item[1] + "</option>"
          );
        });
      } else {
        let client_select = clients.find(client =>
          client[0] == selected_client
        );
        console.log(client_select);
        let customers_select = customers.filter(customer =>
          customer[2] == selected_client
        );

        $(".customer_id_form").empty();
        $(".customer_id_form").append(
          "<option value=''>View All</option>"
        );

        $(".customer_id_form").append(
          "<option value='" + client_select[0] + "'>" + client_select[1] + "</option>"
        );

        $.each(customers_select, function (key, item) {
          $(".customer_id_form").append(
            "<option value='" + item[0] + "'>" + item[1] + "</option>"
          );
        });
      }

    });

  <?php 

    $cities = json_encode($cities);

  ?>
  var cities = <?= $cities ?>;

    $(".state_id_form").change(function () {
      var selected_states = $(this).val();
      if (selected_states == "") {
        $.each(cities, function (key, item) {
          $(".city_id,.city_id_form").append(
            "<option value='" + item[1] + "'>" + item[1] + "</option>"
          );
        });
      } else {
        var cities_select = cities.filter(city =>
          city[0] == selected_states
        );

        $(".city_id").empty();
        $(".city_id").append(
          "<option value=''>View All</option>"
        );
        $(".city_id_form").empty();
        $(".city_id_form").append(
          "<option value=''>View All</option>"
        );
        $.each(cities_select, function (key, item) {
          $(".city_id,.city_id_form").append(
            "<option value='" + item[1] + "'>" + item[1] + "</option>"
          );
        });
      }

    });

  })
</script>