<?php
// Include Functions
include('../../includes/functions.php');
$_GET['mobile'] = false;
$_GET['first_request'] = false;
$_GET['per_page'] = 0;
$_GET['page'] = 0;
$data = postDirReports($_SESSION['access-token'],$_GET);
$sites = $data['sites'];
$clients = $data['clients'];
$checks = $data['checkins'];
//YmdHis
$txtDate = ( $_GET['start'] === $_GET['finished'] )  ? date('Ymd',strtotime($_GET['start'])) : date('Ymd',strtotime($_GET['start']))."-".date('Ymd',strtotime($_GET['finished'])) ;

header("Content-Type: application/vnd.ms-excel");
header("Expires: 0&#8243;");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0&#8243;");
header("content-disposition: attachment;filename=directives-report-".$txtDate.".xls");
?>

<div class="m-section" id="app" >
    <div class="table-responsive">
    <table class=" table table-bordered table-hover table table-bordered table_edit" id="<?php echo $key?>">
        <thead>
          <tr>
            <th>ID</th>
            <th>Site Code</th>
            <th>Client</th>
            <th>Customer</th>
            <th>Site</th>
            <th>Date</th>
            <th width="35px">Status</th>
            <th width="35px">Started At</th>
            <th>Finished At</th>
            <th>Time Elapsed</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Cleaner</th>
            <th>Area Manager</th>
            <th>Supervisor</th>
            <th>Maximum arrival time</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($checks as $key => $branch) { ?>
          <tr <?php if($branch['route_histories_status']==1){echo 'style="background: #eee;"' ;}?>>
            <td scope="row">
              <?= $branch['id'] ?>
            </td>
            <td scope="row">
              <p>
                <?= $branch['site_code'] ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $branch['client'] ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $branch['customer'] ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $branch['site'] ?>
              </p>

            </td>
            <td>
              <p>
              <?php if(empty($branch['date'])){ ?>
                not assigned
                <?php }else{ ?>
                <?php echo $branch['date'] ?>
                <?php } ?>
              </p>
            </td>
            <td>
              <p>
                <?php  
                  switch ($branch['status']) {
                    case '4':
                      $bagde = "danger";
                      $status_msg = "No Show";
                      break;
                    case '3':
                      $bagde = "warning";
                      $status_msg = "Incomplete";
                      break;
                    case '2':
                      $bagde = "info";
                      $status_msg = "Complete UT";
                      break;
                    case '1':
                      $bagde = "success";
                      $status_msg = "Complete";
                      break;
                      
                  }
                ?>
                <span class="m-badge m-badge--<?=$bagde;?> m-badge--wide">
                  <?= $status_msg; ?>
                </span>
              </p>

            </td>
            <td>
              <?php if(empty($branch['start_time'])){ ?>
              No Info
              <?php }else{ ?>
              <span class="m-badge m-badge--secondary m-badge--wide" style="margin-bottom: 5px;width: 90px;">
                <?php echo date('m-d-Y', strtotime($branch['start_time'] ."UTC")); ?>
              </span>
              <span class="m-badge m-badge--secondary m-badge--wide" style="width: 90px;">
                <?php echo date('H:i', strtotime($branch['start_time'] ."UTC")); ?>
              </span>
              <?php } ?>

            </td>
            <td>
              <p>
                <?php if(empty($branch['end_time'])){ ?>
                No Info
                <?php }else{ ?>
                <span class="m-badge m-badge--secondary m-badge--wide" style="margin-bottom: 5px;width: 90px;">
                  <?php echo date('m-d-Y', strtotime($branch['end_time'] ."UTC")); ?>
                </span>
                <span class="m-badge m-badge--secondary m-badge--wide" style="width: 90px;">
                  <?php echo date('H:i', strtotime($branch['end_time'] ."UTC")); ?>
                </span>
                <?php } ?>
              </p>
            </td>
            <td>
              <p>
                <?php echo $branch['time_elapsed'] ?>
              </p>

            </td>
            <td>
              <?= $branch['address'] ?>
            </td>
            <td>
              <?= $branch['city'] ?>
            </td>
            <td>
              <?= $branch['state'] ?>
            </td>
            <td>
              <p>
                <?php if(empty($branch['cleaner_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['cleaner_name'] ?>
                <?php } ?>
              </p>

            </td>

            <td>
              <p>
                <?php if(empty($branch['areamanager_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['areamanager_name']  ?>
                <?php } ?>
              </p>

            </td>

            <td>
              <p>
                <?php if(empty($branch['supervisor_name'])){ ?>
                not assigned
                <?php }else{ ?>
                <?= $branch['supervisor_name']  ?>
                <?php } ?>
              </p>

            </td>
            <td>
              <p>
                <?php echo $date =  date('H:i', strtotime($branch['max_time'])  - 21600);    //$rest = substr($branch['max_time'], 11, 5); echo $rest
                ?>
              </p>

            </td>

          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>

</div>
   

<!-- <script src="../assets/js/jquery.min.js"></script>
<script src="../assets/xlsx.core.min.js"></script>
<script src="../assets/FileSaver.min.js"></script>
<script src="../assets/tableexport.js"></script> -->

<!-- <script>

  /* Defaults */
  var exportTable = document.getElementsByTagName("table")
  var export_tables = new TableExport(exportTable, {
      formats: ['xlsx'],
      bootstrap: true,
      exportButtons: false,
      // ignoreRows: null,             
      // ignoreCols: 10,

  }); 
  var tables_data = export_tables.getExportData()
  var export_data = []
  var xlsx_info = {}
  for (table_id in tables_data){
      xlsx_info = tables_data[table_id]["xlsx"]
      export_data.push(tables_data[table_id]["xlsx"].data)
  }
  var fileExtension = xlsx_info.fileExtension
  var mimeType = xlsx_info.mimeType
  $(document).ready(function(){
      export_tables.exportmultisheet(export_data, mimeType, "Reporte", [],
                                    fileExtension, {}, [])
  })

</script> -->