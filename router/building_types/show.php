<?php
include('../includes/header.php');
$building_type = vcGetServiceType($_SESSION['access-token'], $_GET['id']);
?>

<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
  <?php include('../includes/sidebar_menu.php');?>

  <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Service Type: <?php echo $building_type['name'] ?> </h3>
            </div>
        </div>
    </div>

    <div class="m-content">
      <div class="row">
        <div class="col-xl-12">
          <div class="m-portlet">
            <div class="m-portlet__body m-portlet__body--no-padding">
              <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="m-widget1">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col text-center">
                          <span class="m-widget1__title">Name:</span>
                          <h3 class="m-widget1__title m--font-brand"><?= $building_type['name'] ?></h3>
                        </div>
												</div>
											</div>
										</div>
                  </div>
								</div>
							</div>
						</div>
          </div>
        </div>
     </div>
</div>
