<?php

include('../../includes/functions.php');

$json = file_get_contents('php://input');
$json = json_decode($json, true);

$response = vcCreateBuildingType($_SESSION['access-token'], $json);
echo json_encode($response);

?>
