<?php
include('../includes/header.php');
?>

<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper" id="app">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title ">Import Excel to Database</h3>
        </div>
      </div>

      <!-- END: Subheader -->
      <div class="m-content">
        <!--Begin::Section-->
        <div class="row">
          <div class="col-xl-12 d-flex justify-content-center m-portlet">

            <!--begin::Portlet-->

            <div class="m-portlet__body">

              <button class="btn btn-primary" type="button" :disabled="executing" @click="executeScript">
                <span v-if="executing">
                  <i class="fa fa-spinner fa-spin"></i>
                  Loading
                </span>
                <span v-else>Execute Script</span>
              </button>

            </div>

            <!--end::Portlet-->
            <!--End::Section-->
          </div>
        </div>
        <!--END XL12-->
      </div>
      <!--END ROW-->
    </div>
    <!--End::Content-->
    <!-- end:: Page -->
  </div>
</div>

<?php include('../includes/footer.php'); ?>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>

<script>
  var app = new Vue({
    el: '#app',
    data() {
      return {
        executing: false
      }
    },
    methods: {
      executeScript() {
        this.executing = true;
        axios.post('/router/import-excel/upload-excel.php')
          .then((result) => {
            this.executing = false
            swal({
              title: "Success!",
              text: "The script was executed correctly.",
              icon: "success",
              button: "Ok",
              closeOnEsc: true
            })
          }).catch((err) => {
            this.executing = false
            console.log(err)
          })
      }
    },
  })
  $("button").click(function() {
    var $btn = $(this);
    $btn.button('loading');
    // Make a request for a user with a given ID

  });
</script>
