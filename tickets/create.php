<?php
include('../includes/header.php');
$clients = vcGetClients($_SESSION['access-token']);
$cleaners = vcGetUsers($_SESSION['access-token']); //vcGetCleaners($_SESSION['access-token']);
$active_sites = vcGetActiveBranches($_SESSION['access-token']);

?>


<!-- begin::Body -->
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">


  <?php include('../includes/sidebar_menu.php');?>

  <!-- END: Left Aside -->
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

  <!-- BEGIN: Subheader -->
  <div class="m-subheader ">
      <div class="d-flex align-items-center">
          <div class="mr-auto">
              <h3 class="m-subheader__title ">New Ticket</h3>
              <input type="text" value="<?php echo $_SESSION['id'] ?>" id="user_id" style="display:none">
        
          </div>
      </div>
  </div>

  <!-- END: Subheader -->
  <div class="m-content">
      <!--Begin::Section-->
    <div class="row">
      <div class="col-xl-12">
              
              <!--begin::Portlet-->
        <div class="m-portlet">

          <div class="m-portlet__body">

                      <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right" id="create">
              <div class="m-portlet__body">
                <div class="form-group m-form__group">
                  <label for="clients">Type of Client</label>
                  <select class="form-control m-input" id="type_client" name="type_client" onchange="selectClient()">
                    <option value="">Select One</option>
                    <option value="client" >Client</option>
                    <option value="customer" >Customer</option>
                  </select>
                </div>
                <div class="form-group m-form__group" id="client_content" style="display:none" >
                  <label>Clients</label>
                  <select class="form-control m-input" id="client_id" name="client_id" onchange="sitesForClient()">
                    <option value="" selected>Select One</option>
                  </select>
                </div>
                <div class="form-group m-form__group" id="customer_content" style="display:none" >
                  <label>Customers</label>
                  <select class="form-control m-input" id="customer_id" name="customer_id" onchange="sitesForClient()">
                    <option value="" selected>Select One</option>
                  </select>
                </div>
                <div class="form-group m-form__group" id="branch_id_container" style="display:none">
                  <label>Sites</label>
                  <select class="form-control m-input" id="branch_id" name="branch_id" onchange="$('#div_data_wo').show()" >
                    <option value="" selected>Select One</option>
                  </select>
                </div>
                <div id="div_data_wo" class="hidden">
                  <div class="form-group m-form__group">
                    <label>Request Contact</label>
                    <input type="text" class="form-control m-input" id="request_contact" placeholder="Request Contact" >
                  </div>
                  <div class="form-group m-form__group">
                    <label>Request Contact Email</label>
                    <input type="text" class="form-control m-input" id="request_contact_email" placeholder="Site Contact email" >
                  </div>
                  <div class="form-group m-form__group">
                    <label>Request Contact Phone</label>
                    <input type="text" class="form-control m-input" id="request_contact_phone" placeholder="Request Phone" >
                  </div>
                  <div class="form-group m-form__group">
                    <label>Event Date</label>
                    <input type="date" class="form-control m-input" id="event_date" placeholder="Event Date"  >
                  </div>
                  <div class="form-group m-form__group">
                    <label>Due Date</label>
                    <input type="date" class="form-control m-input" id="due_date" placeholder="Due date" >
                  </div>
                  <div class="form-group m-form__group">
                    <label>Short Description</label>
                    <textarea class="form-control m-input" id="short_description" placeholder="Short Description" rows="4" cols="50" ></textarea  >
                  </div>
                  <div class="form-group m-form__group">
                    <label>Description</label>
                    <textarea class="form-control m-input" id="description" placeholder="Description" rows="4" cols="50" ></textarea  >
                  </div>
                  <div class="form-group m-form__group">
                    <label>Status</label>
                    <select class="form-control m-input" id="status" name="status" >
                      <option value="" selected>Select One</option>
                      <option value="0">Opened</option>
                      <option value="1">In Progress</option>
                      <option value="2">Ready to verify</option>
                      <option value="3">Completed</option>
                    </select>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                      <a onclick="createTicket(event);" class="btn btn-primary">Save</a>
                      <a href="index.php" type="reset" class="btn btn-secondary">Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div> 
        </div> 
      </div>
    </div>
  </div>
<!--END ROW-->
</div>
<!--End::Content-->
<!-- end:: Page -->



<?php include('../includes/footer.php');?>

<!--begin::CLIENTS -->
<script src="../assets/js/plugins/cleave.min.js"></script>
<script src="../assets/js/tickets/create.js" type="text/javascript"></script>
<!--end::CLIENTS -->
